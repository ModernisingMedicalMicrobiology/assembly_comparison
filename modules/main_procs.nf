process yields {
    tag {sample}

    input:
    tuple val(sample), val(refname), file('reads.fastq'), path('meta.csv')

    output:
    path("${sample}.csv")

    script:
    """
    yields.py -s ${sample} -f reads.fastq -m meta.csv \
        -o ${sample}.csv
    """
    stub:
    """
    touch ${sample}.csv
    """

}





process filter {
    tag {sample}

    input:
    tuple val(refname), val(sample), file('reads.fastq'), val(ref)

    output:
    tuple val(sample), val(refname), val(0), file('filt.fastq.gz')

    script:
    """
    filtlong --min_length 1000 \
	--keep_percent 90 \
	--target_bases 500000000  \
	-a $ref \
	reads.fastq | gzip > filt.fastq.gz
    """
    stub:
    """
    touch 'filt.fastq.gz'
    """
}

process randomSampleFQ {
    tag {sample + ' ' + Nreads}

    input:
    tuple val(ref), val(sample), file('reads.fastq'), val(ref), val(genomeSize), val(Nreads)

    output:
    tuple val("${sample}"), val(ref), val("${Nreads}"), path("${sample}_${Nreads}.fastq.gz"), emit: fq

    script:
    """
    rasusa --input reads.fastq --coverage ${Nreads} --genome-size ${genomeSize} | gzip > ${sample}_${Nreads}.fastq.gz
    """ 
    stub:
    """
    touch ${sample}_${Nreads}.fastq.gz
    """
}

process assemble {
    tag {sample + ' ' + sub}

    publishDir "assemblies/${task.process.replaceAll(":","_")}", mode: 'copy', saveAs: { filename -> "${sample}_${sub}.fasta"} 

    input:
    //tuple val(sample), val(refname), val(reads)
    tuple val(sample), val(refname),val(sub), file('reads.fastq.gz')

    output:
    tuple val(sample), val(refname),val(sub), file('assembly/assembly.fasta'), val('flye'), emit: ch_flye
    tuple val(sample), val(sub), val(refname),  file('assembly/assembly.fasta'), val('flye'), emit: ch_flye2


    script:
    """
    flye -o assembly \
	--plasmids \
	--meta \
	--threads ${task.cpus} \
	--nano-hq reads.fastq.gz
    """
    stub:
    """
    mkdir assembly
    touch assembly/assembly.fasta
    """
}

process canu {
    tag {sample + ' ' + sub}

    publishDir "assemblies/${task.process.replaceAll(":","_")}", mode: 'copy', saveAs: { filename -> "${sample}_${sub}.fasta"} 

    input:
    //tuple val(sample), val(refname), file('reads.fastq.gz')
    tuple val(refname), val(sample), val(sub), file('reads.fastq'), val(refname), val(genomeSize)

    output:
    tuple val(sample), val(refname),val(sub), file("${sample}/${sample}.contigs.fasta"), val('canu'), emit: ch_canu

    script:
    """
    canu \
	-p ${sample} -d ${sample} \
	maxThreads=${task.cpus} \
	useGrid=false \
	genomeSize=$genomeSize maxInputCoverage=100 \
	-nanopore 'reads.fastq'
    """
    stub:
    """
    mkdir ${sample}
    touch ${sample}/${sample}.contigs.fasta
    """
}

process unicycler {
    tag {sample + ' ' + sub}

    publishDir "assemblies/${task.process.replaceAll(":","_")}", mode: 'copy', saveAs: { filename -> "${sample}_${sub},.fasta"}
    //publishDir "assemblies/${task.process.replaceAll(":","_")}_spades", mode: 'copy', pattern:"*contigs.fasta", saveAs: { filename -> "${sample}.fasta"}

    input:
    tuple val(refname), val(sample), val(sub), file('reads.fastq.gz'), file('reads1.fastq.gz'), file('reads2.fastq.gz')

    output:
    tuple val(sample), val(refname),val(sub), path("${sample}_unicycler/assembly.fasta"), val('unicycler'), emit: ch_uc
    tuple val(sample), val(refname),val(sub), path("${sample}_unicycler/spades_assembly/assembly_graph_with_scaffolds.gfa"), emit: spades_graph
    tuple val(sample), val(refname),val(sub), path("${sample}_unicycler/spades_assembly/contigs.fasta"), val('spades'), emit: spades_fasta


    script:
    """
    unicycler -1 'reads1.fastq.gz' \
              -2 'reads2.fastq.gz' \
              -l reads.fastq.gz \
              -o ${sample}_unicycler \
              --keep 3 \
              -t ${task.cpus}
    """
    stub:
    """
    mkdir -P "${sample}_unicycler/spades_assembly/
    touch ${sample}_unicycler/spades_assembly/assembly_graph_with_scaffolds.gfa
    touch ${sample}_unicycler/spades_assembly/contigs.fasta
    """
}

process polish {
    label 'medaka'
    tag {sample + ' ' + sub}

    publishDir "assemblies/${task.process.replaceAll(":","_")}", mode: 'copy', saveAs: { filename -> "${sample}_${sub}.fasta"}

    input:
    tuple val(sample), val(refname),val(sub), path('contigs.fasta'), val(assembler), path('reads.fastq.gz'), val(model)

    output:
    tuple val(sample), val(sub), val(refname), path('output/consensus.fasta'), val(assembler), emit: fasta
    tuple val(sample), val(refname),val(sub), path('output/consensus.fasta'), val(assembler), emit: fasta2

    script:
    """
    medaka_consensus \
	-i reads.fastq.gz \
	-d contigs.fasta \
	-o output \
	-t ${task.cpus} \
	-m ${model}
    """
    stub:
    """
    mkdir output
    touch output/consensus.fasta
    """
}


process makeblastdb {
    tag {refname}

    input:
    tuple val(refname), val(ref)

    output:
    tuple file('ref.fa'), val(refname),  file('ref.fa.ndb'), file('ref.fa.nhr'), file('ref.fa.nin'), file('ref.fa.nog'), file('ref.fa.nos'), file('ref.fa.not'), file('ref.fa.nsq'),  file('ref.fa.ntf'),  file('ref.fa.nto')

    script:
    """

    cp $ref ref.fa

    makeblastdb -in ref.fa \
	-parse_seqids \
	-blastdb_version 5 \
	-title ${refname} \
	-dbtype nucl
    """
    stub:
    """
    touch ref.fa ref.fa.ndb ref.fa.nhr ref.fa.nin ref.fa.nog ref.fa.nos ref.fa.not ref.fa.nsq ref.fa.ntf ref.fa.nto
    """
}

process blast {
    tag {sample + ' ' + sub}

    publishDir "blastn/${assembler}", mode: 'copy'

    input:
    tuple val(refname), val(sample), val(sub), file('assembly.fasta'), val(assembler), file('ref.fa'), file('ref.fa.ndb'), file('ref.fa.nhr'), file('ref.fa.nin'), file('ref.fa.nog'), file('ref.fa.nos'), file('ref.fa.not'), file('ref.fa.nsq'),  file('ref.fa.ntf'),  file('ref.fa.nto')

    output:
    tuple val(sample),val(sub), file("${sample}_${sub}.blast.tsv")

    script:
    """
    blastn -query assembly.fasta -db ref.fa -outfmt 6 > ${sample}_${sub}.blast.tsv
    """
    stub:
    """
    touch ${sample}_${sub}.blast.tsv
    """
}

process dnadiff {
    tag {sample + ' ' + sub}

    publishDir "dnadiff/${assembler}", mode: 'copy'
    publishDir "snps/${assembler}", pattern: '*.snps', mode: 'copy', saveAs: { filename -> "${sample}_${sub}.snps"}

    input:
    tuple val(refname), val(sample), val(sub), file('assembly.fasta'), val(assembler), file('ref.fa')

    output:
    tuple val(sample), val(assembler), val(sub), file("${sample}_${sub}.report"), file("${sample}_${sub}.delta"), file("${sample}_${sub}.1coords"), emit: all
    tuple val(sample), val(assembler), val(sub), file("${sample}_${sub}.1coords"), emit: coords
    tuple val(sample), val(assembler), val(sub), file("${sample}_${sub}.snps"), emit: snps


    script:
    """
    dnadiff ref.fa assembly.fasta -p ${sample}_${sub}
    """
    stub:
    """
    touch ${sample}_${sub}.report ${sample}_${sub}.delta ${sample}_${sub}.1coords
    """
}

process dnadiff_rev {
    tag {sample + ' ' + sub}

    publishDir "dnadiff_rev/${assembler}", mode: 'copy'
    publishDir "snps_rev/${assembler}", pattern: '*.snps', mode: 'copy', saveAs: { filename -> "${sample}_${sub}.snps"}

    input:
    tuple val(refname), val(sample), val(sub), file('assembly.fasta'), val(assembler), file('ref.fa')

    output:
    tuple val(sample), val(assembler), val(sub), file("${sample}_${sub}.report"), file("${sample}_${sub}.delta"), file("${sample}_${sub}.1coords"), emit: all
    tuple val(sample), val(assembler), val(sub), file("${sample}_${sub}.1coords"), emit: coords
    tuple val(sample), val(assembler), val(sub), file("${sample}_${sub}.snps"), emit: snps


    script:
    """
    dnadiff  assembly.fasta ref.fa -p ${sample}_${sub}
    """
    stub:
    """
    touch ${sample}_${sub}.report ${sample}_${sub}.delta ${sample}_${sub}.1coords
    """
}

process minimap2 {
    tag {sample + ' ' + sub }

    publishDir "bams", pattern: '*.bam', saveAs: { filename -> "${sample}_${sub}.sorted.bam" }

    input:
	tuple val(refname), val(sample), val(sub), file('reads.fastq'), val(ref)

    output:
	tuple val(sample), val(sub), file("${sample}.sorted.bam"), val(ref)


    script:
    """
    cp ${ref} ref.fa
    minimap2 -t ${task.cpus} -ax map-ont ref.fa 'reads.fastq' |\
        samtools view -b - | samtools sort -o ${sample}.sorted.bam 
    """
    stub:
    """
    touch "${sample}.sorted.bam"
    """
}

process mapstats {
    tag { sample + ' ' + sub }

    publishDir 'mapcsvs', mode: 'copy'

    input:
	tuple val(sample), val(sub), file("${sample}.sorted.bam"), val(ref)

    output:
    tuple val(sample), val(sub),  file("${sample}_${sub}.read_stats.csv")

    script:
    """
    samtools index ${sample}.sorted.bam
    bamreadstats.py ${sample}.sorted.bam ${sample}_${sub}
    """
    stub:
    """
    touch ${sample}_${sub}.read_stats.csv
    """
}

process subsample {
    tag { sample + sub}
    publishDir "bams/${task.process.replaceAll(":","_")}", mode: 'copy'
    
    input:
        tuple val(sample), path("${sample}.sorted.bam"), val(ref), val(sub)

    output: 
        tuple val(sample), path("${sample}.${sub}.sorted.bam"), path("${sample}.${sub}.sorted.bam.bai"),val(ref), val(sub)

    script:
    """
    samtools index ${sample}.sorted.bam
    subSampleBam.py -b ${sample}.sorted.bam -s $sub -o ${sample}.${sub}.sorted.bam
    """
    stub:
    """
    touch ${sample}.${sub}.sorted.bam ${sample}.${sub}.sorted.bam.bai
    """
}


process medaka {
    tag { sample + ' ' + sub}
    
    publishDir "vcfs/${task.process.replaceAll(":","_")}", mode: 'copy', pattern: '*.vcf'

    input:
    tuple val(sample), path("${sample}.sorted.bam"), path("${sample}.${sub}.sorted.bam.bai"), val(ref), val(sub), val(model)

    output: 
	tuple val(sample), val(sub), path("${sample}_${sub}.vcf"), path("${sample}.sorted.bam"), path("${sample}.sorted.bam.bai"), val(ref)

    script:
    """
    samtools index ${sample}.sorted.bam
    medaka consensus --model $model --save_features ${sample}.sorted.bam medaka_cons.hdf 
    medaka variant $ref medaka_cons.hdf ${sample}_${sub}.vcf
    """
    stub:
    """
    touch ${sample}_${sub}.vcf ${sample}.sorted.bam ${sample}.sorted.bam.bai
    """
}

process NAMESORT_BAM {
    tag {sample + ' ' + sub}
    label 'map'
    cpus=1

    input:
    tuple val(sample), val(refname), val(sub), path('mapped.bam'), path('ref.fa')
    
    output:
    tuple val(sample), val(refname), val(sub), path("${sample}.bam")

    script:
    """
    samtools sort -n -o ${sample}.bam mapped.bam
    """
}

process REPAIR_BAM {
    tag {sample + ' ' + sub}
    label 'map'
    cpus=4
    publishDir "logs/${task.process.replaceAll(":","_")}", pattern:'*.log'

    input:
    tuple val(sample), val(refname), val(sub), path("${sample}.aln.bam"), path("${sample}.mod.bam")
    
    output:
    tuple val(sample), val(refname), val(sub), path("${sample}.bam"), emit: bam
    tuple val(sample), val(refname), val(sub), path("${sample}.log"), emit: log


    script:
    """
    /mnt/data/soft/modkit/dist/modkit repair \
        -d ${sample}.mod.bam \
        -a ${sample}.aln.bam \
        -o ${sample}.bam \
        --log-filepath ${sample}.log \
        -t ${task.cpus}
    """
}

process POS_SORT_BAM {
    tag {sample + ' ' + sub}
    label 'map'
    cpus=1

    input:
    tuple val(sample), val(refname), val(sub), path('mapped.bam')
    
    output:
    tuple val(sample), val(refname), val(sub), path("${sample}.bam"), path("${sample}.bam.bai")

    script:
    """
    samtools sort -o ${sample}.bam mapped.bam
    samtools index ${sample}.bam
    """
}

process MODKIT {
    tag {sample + ' ' + sub}
    label 'map'

    cpus 4

    publishDir "beds/${task.process.replaceAll(":","_")}"

    input:
    tuple val(sample), val(refname), val(sub), path("${sample}.bam"), path("${sample}.bam.bai")

    output:
    tuple val(sample), val(refname), val(sub), path("${sample}_${sub}.bed"), emit: bed

    script:
    """
    /mnt/data/soft/modkit/dist/modkit pileup \
        ${sample}.bam \
        ${sample}.bed \
        -t ${task.cpus} 

    """
    stub:
    """
    touch ${sample}.bed
    """
}

process forest_filter {
    tag { sample + ' ' + sub}
    label 'forest_filter'
    cpus 2
    
    publishDir "vcfs/${task.process.replaceAll(":","_")}", mode: 'copy', pattern: '*.vcf'

    input:
    tuple val(sample), val(refname), val(sub), path('ref.fa'), val(inputassembler), path("reads.fastq"), path('model.sav')

    output: 
	tuple val(sample), val(sub), path("${sample}_${sub}.fasta"), emit: fasta
    tuple val(sample), val(refname),val(sub), path("${sample}_${sub}.fasta"), val('forest_filter'), emit: fasta2


    script:
    """
    minimap2 -t ${task.cpus} -ax map-ont ref.fa 'reads.fastq' |\
        samtools view -b - | samtools sort -o ${sample}_${sub}.sorted.bam 
    samtools index ${sample}_${sub}.sorted.bam 

    forest_filter.py -r ref.fa -b ${sample}_${sub}.sorted.bam -o ${sample}_${sub}.fasta -m model.sav
    """
    stub:
    """
    touch ${sample}_${sub}.fasta
    """
}


process filterSNPs {
    tag{ sample + ' ' + sub }

    publishDir "vcfs/${task.process.replaceAll(":","_")}", mode: 'copy', pattern: '*.vcf'

    input:
	tuple val(sample), val(sub), path("${sample}_${sub}.vcf"), path("${sample}.sorted.bam"), path("${sample}.sorted.bam.bai"), val(ref), val(modelName), val(model), val(filtermode), val(combination)

    output:
        tuple val(sample), val(sub), path("${sample}_${sub}_${modelName}.vcf")

    script:
    
    """
    python3 /home/nick/soft/forest_filter/forest_filter.py classify \
	-v ${sample}_${sub}.vcf \
	-b ${sample}.sorted.bam \
	-r $ref \
	-m $model \
	-fm $filtermode \
	-c $combination \
	-o ${sample}_${sub}_${modelName}.vcf
    """
    stub:
    """
    touch ${sample}_${sub}_${modelName}.vcf
    """
}

process snippy {
    tag { sample }

    publishDir "vcfs/${task.process.replaceAll(":","_")}", mode: 'copy'

    input:
	tuple val(sample), file('reads1.fastq'), file('reads2.fastq'), val(ref)

    output:
	tuple val(sample), file("${sample}_output"), emit: ch_out
	tuple val(sample), val(0), file("${sample}_output/snps.bam"), val(ref), emit: bam
	tuple val(sample), file("${sample}_output/snps.consensus.fa"), val(ref), emit: fasta 
	tuple val(sample), file("${sample}_output/snps.consensus.fa"), emit: correctedRef 
    tuple file("${sample}_output/snps.consensus.fa"), val(sample), emit: correctedRef2
	tuple val(sample), val(sample), val(0), file("${sample}_output/snps.consensus.fa"), val('snippy'), emit: fasta2 
	
    script:
    """
    snippy --cpus $task.cpus \
	--outdir ${sample}_output \
	--ref ${ref} \
	--R1 'reads1.fastq' \
	--R2 'reads2.fastq'
    """
}

process prokka {
    tag { sample + ' ' + sub}
    
    publishDir "prokka/${task.process.replaceAll(":","_")}"

    cpus=8

    input:
        tuple val(refname), val(sample), val(sub), file("consensus.fa"), val(assembler), val(ref.gbk)

    output:
        tuple val(refname), val(sample), val(sub), path("${sample}_${sub}_${assembler}"), emit: ch_out
        tuple val(refname), val(sample), val(sub), path("${sample}_${sub}_${assembler}/${sample}_${sub}.gbk"), val(assembler), emit: gbk

    script:
    """
    prokka --cpus ${task.cpus} \
	--proteins ref.gbk \
        --addgenes \
        --force \
        --compliant \
        --outdir ${sample}_${sub}_${assembler} \
        --prefix ${sample}_${sub} consensus.fa
    """
    stub:
    """
    mkdir ${sample}_${sub}_${assembler}
    touch ${sample}_${sub}_${assembler}/${sample}_${sub}.gbk 
    """
}


process compareProkka {
    tag { sample + ' ' + sub + ' ' + assembler}

    publishDir "gbk_compare/${task.process.replaceAll(":","_")}", mode: 'copy'

    input:
	tuple val(refname), val(sample), val(sub), path("test.gbk"), val(assembler), val(test_sample), val(test_subsample), path("ref.gbk"), val(ref_assembler)
		
    output:
    tuple val(sample), val(sub), val(assembler), path("${sample}_${sub}_${assembler}.csv")

    script:
    """
    AAcompare.py -rg ref.gbk \
	-tg test.gbk \
        -o ${sample}_${sub}_${assembler}.csv \
	-n ${assembler} \
	-s ${sample}
    """
    stub:
    """
    touch ${sample}_${sub}_${assembler}.csv
    """
}

process seqkitNanopore {
    tag {sample + ' ' + sub}

    publishDir "CSVs/${task.process.replaceAll(":","_")}", mode: 'copy'

    input:
    tuple val(sample), val(ref), val(sub), file('reads')

    output:
    tuple val(sample), val(sub), path("${sample}_${sub}.tsv")

    script:
    """
    seqkit stats -T --all reads > ${sample}_${sub}.tsv
    """
    stub:
    """
    touch ${sample}_${sub}.tsv
    """
}

process seqkitIllumina {
    tag {sample}

    publishDir "CSVs/${task.process.replaceAll(":","_")}", mode: 'copy'

    input:
    tuple val(sample), file('reads1.fastq'), file('reads2.fastq')

    output:
    tuple val(sample), val(0), path("${sample}.tsv")

    script:
    """
    seqkit stats -T --all reads1.fastq reads2.fastq > ${sample}.tsv
    """
    stub:
    """
    touch  ${sample}.tsv
    """
}

process convertGraph {
    tag {sample + ' ' + sub}
    publishDir "assemblies/${task.process.replaceAll(":","_")}_spades", mode: 'copy', pattern:'*.fasta', saveAs: { filename -> "${sample}_${sub}.fasta"}

    input:
    tuple val(sample), val(refname), val(sub), path('contigs.gfa')

    output:
    tuple val(sample), val(refname), val(sub), path("contigs.fasta"), val('spades')

    script:
    """
    gfa2fasta.bash contigs.gfa > contigs.fasta
    """
    stub:
    """
    touch contigs.fasta
    """
}

process seqkitContigs {
    tag {sample + ' ' + sub + ' ' + assembler}

    publishDir "CSVs/${task.process.replaceAll(":","_")}", mode: 'copy'

    input:
    tuple val(sample), val(refname), val(sub), path('contigs.fasta'), val(assembler)

    output:
    tuple val(sample),val(sub), val(assembler), path("${sample}_${sub}_${assembler}.tsv")

    script:
    """
    seqkit stats -T --all contigs.fasta > ${sample}_${sub}_${assembler}.tsv
    """
    stub:
    """
    touch ${sample}_${sub}_${assembler}.tsv
    """
}



process quast {
    tag {refname}

    publishDir "quast/${task.process.replaceAll(":","_")}", mode: 'copy'

    input:
    tuple val(refname), val(samples), val(sub), path('contigs??.fasta'), val(assemblers), path('ref.fa'), path('ref.gff3')

    output:
    tuple val(refname), path("${refname}_output")

    script:
    lables=[samples, sub, assemblers].transpose().collect { "${it[0]}_${it[1]}_${it[2]}" }
    labels=lables.join(",")
    """
    echo $samples > out.txt
    quast --glimmer  -o ${refname}_output \
	--features gene:ref.gff3 \
	-r ref.fa \
	-l $labels \
	contigs*.fasta \
	--circos
    """
    stub:
    """
    mkdir ${refname}_output
    """
}

process raw_sequencing_stats {
    tag {sample + ' ' + sub}

    input:
    tuple val(sample), val(sub), path("${sample}.tsv"), path('meta.csv')

    output:
    path("${sample}_${sub}.csv")

    script:
    """
    raw_sequencing_stats.py -i ${sample}.tsv --sample_name ${sample} \
        --subsample ${sub} \
        -o ${sample}_${sub}.csv -m meta.csv
    """
    stub:
    """
    touch ${sample}_${sub}.csv
    """

}

process parse_dnadiff {
    tag {sample + ' ' + sub + ' ' + assembler}

    input:
    tuple val(sample), val(assembler), val(sub), file("${sample}.report"), file("${sample}.delta"), file("${sample}.1coords"), path('meta.csv')

    output:
    path("${sample}_${sub}_${assembler}.csv")

    script:
    """
    parse_dnadiff_report.py --sample_name ${sample} \
        -a ${assembler} \
        --subsample ${sub} \
        -i ${sample}.report \
        -o ${sample}_${sub}_${assembler}.csv -m meta.csv
    """
    stub:
    """
    touch ${sample}_${sub}_${assembler}.csv
    """
}

process parse_dnadiff_SNPs {
    tag {sample + ' ' + sub + ' ' + assembler}

    input:
    tuple val(sample), val(assembler), val(sub), file("${sample}.snps"), path('meta.csv')

    output:
    path("${sample}_${sub}_${assembler}.csv")

    script:
    """
    parse_dnadiff_snps.py --sample_name ${sample} \
        -a ${assembler} \
        --subsample ${sub} \
        -i ${sample}.snps \
        -o ${sample}_${sub}_${assembler}.csv -m meta.csv
    """
    stub:
    """
    touch ${sample}_${sub}_${assembler}.csv
    """
}

process prokka_genes_found {
    tag {sample + ' ' + sub + ' ' + assembler}

    input:
    tuple val(sample), val(sub), val(assembler), path("${sample}_${assembler}.csv"), path('meta.csv')

    output:
    path("${sample}_${sub}_${assembler}_genes.csv")

    script:
    """
    prokka_genes_found.py --sample_name ${sample} \
        --subsample ${sub} \
        -a ${assembler} \
        -i ${sample}_${assembler}.csv \
        -o ${sample}_${sub}_${assembler}_genes.csv \
        -m meta.csv
    """
    stub:
    """
    touch "${sample}_${sub}_${assembler}_genes.csv"
    """

}

process raw_read_stats {
    tag {sample + ' ' + sub}

    input:
    tuple val(sample), val(sub), file("${sample}.read_stats.csv"), path('meta.csv')

    output:
    path("${sample}_${sub}.csv")

    script:
    """
    raw_read_stats.py --sample_name ${sample} \
        --subsample ${sub} \
        -i ${sample}.read_stats.csv \
        -o ${sample}_${sub}.csv -m meta.csv
    """
    stub:
    """
    touch ${sample}_${sub}.csv
    """

}

process dnadiff_recovery {
    tag {sample + ' ' + sub + ' ' + assembler}

    input:
    tuple val(sample), val(assembler),val(sub), path("${sample}.1coords"), path('meta.csv')

    output:
    path("dnadiff_ref_recovery.csv"), emit: refs
    path("dnadiff_plasmid_recovery.csv"), emit: plasmid

    script:
    """
    dnadiff_recovery.py --sample_name ${sample} --subsample ${sub} -a ${assembler} \
     -i ${sample}.1coords -m meta.csv
    """
    stub:
    """
    touch dnadiff_ref_recovery.csv dnadiff_plasmid_recovery.csv
    """

}

process duplex_stats {
    publishDir 'results', mode: 'copy', pattern: '*.csv'
    //publishDir 'rough_plots', mode: 'copy', pattern: '*.pdf'

    input:
    path('raw_sequencing_stats.csv')

    output:
    path("duplex_bases_stats.csv"), emit: bases
    path("duplex_reads_stats.csv"), emit: reads

    script:
    """
    duplex_stats.py
    """
    stub:
    """
    touch duplex_bases_stats.csv duplex_reads_stats.csv total_reads_bar.pdf total_bases_bar.pdf
    """

}