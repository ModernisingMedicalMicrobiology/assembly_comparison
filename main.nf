#!/usr/bin/env nextflow

// enable dsl2
nextflow.enable.dsl=2

// include modules
include {filter} from './modules/main_procs.nf'
include {assemble} from './modules/main_procs.nf'
include {polish; polish as polish2} from './modules/main_procs.nf'
include {polish as polish3} from './modules/main_procs.nf'
include {forest_filter} from './modules/main_procs.nf'
include {makeblastdb} from './modules/main_procs.nf'
include {blast} from './modules/main_procs.nf'
include {dnadiff} from './modules/main_procs.nf'
include {dnadiff_rev} from './modules/main_procs.nf'

include {minimap2; minimap2 as minimap2_self} from './modules/main_procs.nf'
include {mapstats} from './modules/main_procs.nf'
include {subsample} from './modules/main_procs.nf'
include {medaka} from './modules/main_procs.nf'
include {filterSNPs} from './modules/main_procs.nf'
include {snippy} from './modules/main_procs.nf'
include {unicycler} from './modules/main_procs.nf'
include {seqkitIllumina} from './modules/main_procs.nf'
include {seqkitNanopore} from './modules/main_procs.nf'
include {seqkitContigs; seqkitContigs as seqkitContigsRef} from './modules/main_procs.nf'
include {convertGraph} from './modules/main_procs.nf'
include {quast} from './modules/main_procs.nf'
include {canu} from './modules/main_procs.nf'
include {prokka; prokka as prokkaSNIPPY} from './modules/main_procs.nf'
include {compareProkka} from './modules/main_procs.nf'
include {randomSampleFQ} from './modules/main_procs.nf'
include {raw_sequencing_stats} from './modules/main_procs.nf'
include {parse_dnadiff} from './modules/main_procs.nf'
include {parse_dnadiff_SNPs} from './modules/main_procs.nf'

include {prokka_genes_found} from './modules/main_procs.nf'
include {raw_read_stats} from './modules/main_procs.nf'
include {dnadiff_recovery} from './modules/main_procs.nf'
include {yields} from './modules/main_procs.nf'
include {duplex_stats} from './modules/main_procs.nf'



params.meta="${projectDir}/meta/sample_meta.csv"
params.assembly_meta="${projectDir}/meta/sample_assembly_meta.csv"



// main workflow
workflow standard{
    take:
        ch_fqs
	ch_refs
	ch_fqsIllumina
	ch_features
	ch_gbks
	Nreads
	refSizes
	medaka_models
        ch_meta
        rf_model
    main:
        // maintain full fastq for seqkitNanopore 
        ch_full=ch_fqs.map{row -> tuple(row[0], row[1], 'full', row[2])}

	// correct references with Illumina
	snippy(ch_fqsIllumina.combine(ch_refs, by:0))

	// get raw data information
	seqkitIllumina(ch_fqsIllumina)

        yields(ch_fqs.combine(ch_meta))

	// Run Filtlong to cap top number of bases
	ch_fqs2=ch_fqs.map{row -> tuple(row[1], row[0], row[2])}

        filter(ch_fqs2.combine(snippy.out.correctedRef, by:0))
	
	// random subsample FQs to number of reads
        subChannel=ch_fqs
		.combine(refSizes, by:1)
		.combine(Nreads)

        randomSampleFQ(subChannel)

        

        ch_fqs2=filter.out.mix(randomSampleFQ.out.fq, ch_full)

        seqkitNanopore(ch_fqs2)

        ch_fqs2=ch_fqs2.filter({ "${it[2]}" != 'full' })

        ch_fqs=ch_fqs2.map{row -> tuple(row[1], row[0], row[2], row[3])}

	
    
	// Assemble
        assemble(ch_fqs2)

	// ### polish 3x with medaka, save each
        polish_in=assemble.out.ch_flye
			.combine(ch_fqs2, by:[0,1,2])
			.combine(medaka_models, by:0)
			.map{row -> tuple(row[0],row[1],row[2],row[3],'flye_medaka',row[5],row[6])}
                        //.view()

        polish(polish_in)

        polish_in2=polish.out.fasta2
			.combine(ch_fqs2, by:[0,1,2])
			.combine(medaka_models, by:0)
			.map{row -> tuple(row[0],row[1],row[2],row[3],'flye_medakaX2',row[5],row[6])}

	polish2(polish_in2)

        polish_in3=polish2.out.fasta2
			.combine(ch_fqs2, by:[0,1,2])
			.combine(medaka_models, by:0)
			.map{row -> tuple(row[0],row[1],row[2],row[3],'flye_medakaX3',row[5],row[6])}

	polish3(polish_in3)

        //minimap2_self(ch_fqs2.combine(assemble.out.ch_flye, by:[0,1,2]))

        //NAMESORT_BAM(minimap2_self.out)

        //REPAIR_BAM(NAMESORT_BAM.out.combine(modbams, by: 0))

        //POS_SORT_BAM(REPAIR_BAM.out)

        //MODKIT(POS_SORT_BAM.out)

        forest_filter(assemble.out.ch_flye
                .combine(ch_fqs2, by:[0,1,2] )
                .combine(rf_model))

	// #### run other assemblers        
	//canu(ch_fqs2.combine(refSizes,by:1))

	ch_fqs.map{row -> tuple(row[0], row[1], row[2], row[3])}
                .filter({ it[1] =~ /r9_sup/ })
                .filter({ it[2] == '50' })
    		.set{ ch_filterRemix }

        unicycler(ch_filterRemix.combine(ch_fqsIllumina, by:0))


	// some utils
        convertGraph(unicycler.out.spades_graph)
    
        makeblastdb(snippy.out.correctedRef)

//        assembled_contigs=assemble.out.ch_flye.mix(unicycler.out.ch_uc,unicycler.out.spades_fasta,canu.out.ch_canu,polish.out.fasta2, polish2.out.fasta2, polish3.out.fasta2)
        assembled_contigs=assemble.out.ch_flye.mix(unicycler.out.ch_uc,unicycler.out.spades_fasta,polish.out.fasta2, polish2.out.fasta2, polish3.out.fasta2, forest_filter.out.fasta2)


	// Compare and annotate
        blast(assembled_contigs.combine(makeblastdb.out, by:1))
        
	dnadiff(assembled_contigs.combine(snippy.out.correctedRef2, by:1))

        dnadiff_rev(assembled_contigs.combine(snippy.out.correctedRef2, by:1))

        //all_contigs=assemble.out.ch_flye.mix(unicycler.out.ch_uc, unicycler.out.spades_fasta, canu.out.ch_canu)
        all_contigs=assemble.out.ch_flye.mix(unicycler.out.ch_uc, unicycler.out.spades_fasta)

	seqkitContigs(all_contigs)

	ch_sk_refs=snippy.out.correctedRef
		.map{t -> tuple(t[0], t[0], 0, t[1], 'reference')}

	seqkitContigsRef(ch_sk_refs)
    
        minimap2(ch_fqs.combine(snippy.out.correctedRef, by:0))
    
	prokkaSNIPPY(snippy.out.fasta2.combine(ch_gbks, by:0))

        assembled_contigs_x=assembled_contigs
			.map{row->tuple(row[1],row[0],row[2],row[3], row[4])}
                        //.view()

	prokka(assembled_contigs_x.combine(ch_gbks, by:0))

        compareProkka(prokka.out.gbk.combine(prokkaSNIPPY.out.gbk, by:0))

        bam_ch=minimap2.out.mix(snippy.out.bam)

        mapstats(bam_ch)

        groupContigs=assembled_contigs
		.groupTuple(by:1)
		.map{row -> tuple(row[1],row[0],row[2],row[3],row[4])}
		.combine(ch_refs, by:0)
		.combine(ch_features, by:0)

	//quast(groupContigs)


        // ######### generate merged output files ########

        // ## seqkit raw_sequencing_stats
        seqkitNanoporeCombined=seqkitNanopore.out.mix(seqkitIllumina.out)

        raw_sequencing_stats(seqkitNanoporeCombined.combine(ch_meta))

        raw_sequencing_stats.out.collectFile(name:'raw_sequencing_stats.csv',
              storeDir:"results/" ,
              keepHeader:true,
              sort:true,
              skip:1)
              .set{raw_sequencing_stats}

        duplex_stats(raw_sequencing_stats)

        // ## dnadiff dnadiff.csv
        parse_dnadiff(dnadiff.out.all.combine(ch_meta))

        parse_dnadiff_SNPs(dnadiff.out.snps.combine(ch_meta))


        parse_dnadiff.out.collectFile(name:'dnadiff.csv',
              storeDir:"results/",
              keepHeader:true,
              sort:true,
              skip:1)
              .set{dnadiff_parsed}

        parse_dnadiff_SNPs.out.collectFile(name:'dnadiff_SNPs.csv',
              storeDir:"results/",
              keepHeader:true,
              sort:true,
              skip:1)
              .set{dnadiff_parsed_SNPs}

        // ## prokka genes

        prokka_genes_found(compareProkka.out.combine(ch_meta))

        prokka_genes_found.out.collectFile(name:'genes_found_flowcell.csv',
              storeDir:"results/",
              keepHeader:true,
              sort:true,
              skip:1)
              .set{genes_found_flowcell}

        // ## raw read stats

        raw_read_stats(mapstats.out.combine(ch_meta))

        raw_read_stats.out.collectFile(name:'raw_read_stats.csv',
              storeDir:"results/",
              keepHeader:true,
              sort:true,
              skip:1)
        
        // ## ref recovery

        dnadiff_recovery(dnadiff.out.coords.combine(ch_meta))

        dnadiff_recovery.out.refs.collectFile(name:'dnadiff_ref_recovery.csv',
              storeDir:"results/",
              keepHeader:true,
              sort:true,
              skip:1)

        dnadiff_recovery.out.plasmid.collectFile(name:'dnadiff_plasmid_recovery.csv',
              storeDir:"results/",
              keepHeader:true,
              sort:true,
              skip:1)

        // ## yields
        yields.out.collectFile(name:'all_sampled_meta.csv',
              storeDir:"results/",
              keepHeader:true,
              sort:true,
              skip:1)

}


workflow mutated{
    take:
        ch_fqs2
        ch_mut_refs
        depths
	models
	filterModels
	ch_fqsIllumina
    main:

        minimap2(ch_fqs2.combine(ch_mut_refs, by:0))

	snippy(ch_fqsIllumina.combine(ch_mut_refs, by:0))

        subsample(minimap2.out.combine(depths))

        medaka(subsample.out.combine(models, by:0))

        filterOPs=Channel.from('SNPs_only','composite')
		.collect()
        filterSNPs(medaka.out.combine(filterModels, by:0).combine(filterOPs))
}

workflow indels{
    take:
        ch_fqs2
        ch_indel_refs
        depths
	models
	filterModels
	ch_fqsIllumina
    main:

        minimap2(ch_fqs2.combine(ch_indel_refs, by:0))

	snippy(ch_fqsIllumina.combine(ch_indel_refs, by:0))

        subsample(minimap2.out.combine(depths))

        medaka(subsample.out.combine(models, by:0))

        filterOPs=Channel.from('INDELs_only','compositeINDELs')
		.collect()
        filterSNPs(medaka.out.combine(filterModels, by:0).combine(filterOPs))
}

workflow {
    // channels
    Channel.fromPath( "${params.inputFastq}" )
           .splitCsv()
           .map { row -> tuple(row[0], row[1], file(row[2])) }
           .set{ ch_fqs }

    Channel.fromPath( "${params.inputRFmodel}" )
           .set{ rf_model }

    Channel.fromPath( "${params.inputIlluminaFastq}" )
           .splitCsv()
           .map { row -> tuple(row[0], file(row[1]), file(row[2])) }
           .set{ ch_fqsIllumina }
    
    Channel.fromPath( "${params.inputRefs}" )
           .splitCsv()
           .map { row -> tuple(row[0], "${workflow.projectDir}/${row[1]}") }
           .set{ ch_refs }

    Channel.fromPath( "${params.meta}" )
            .set{ ch_meta }

    Channel.fromPath( "${params.inputRefSizes}" )
           .splitCsv()
           .map { row -> tuple(row[0] ,row[0], row[1]) }
           .set{ ch_refs_sizes }

    Channel.fromPath( "${params.inputGBKs}" )
           .splitCsv()
           .map { row -> tuple(row[0], "${workflow.projectDir}/${row[1]}") }
           .set{ ch_gbks }

    Channel.from([10, 20, 30, 40, 50, 100])
           .set{Nreads}

    Channel.fromPath( "${params.inFeatures}" )
           .splitCsv()
           .map { row -> tuple(row[0], "${workflow.projectDir}/${row[1]}") }
           .set{ ch_features }

    Channel.fromPath( "${params.medaka_models}" )
           .splitCsv()
           .map { row -> tuple(row[0] ,row[1]) }
           .set{ medaka_models }

    
    Channel
          .from( 2, 5, 10, 20, 50, 100 )
          .set{ depths }

//    Channel
//           .from( 10, 20 )
//           .set{ depths }
    
    Channel.fromPath( "${params.medaka_models}" )
           .splitCsv()
           .map { row -> tuple(row[0] ,row[1]) }
           .combine(Nreads)
           .map { t -> tuple("${t[0]}_${t[2]}",t[1]) }
           .set{ medaka_models_depths }

    medaka_models=medaka_models.mix(medaka_models_depths)


    main:

        standard(ch_fqs, ch_refs, ch_fqsIllumina, ch_features, ch_gbks, Nreads, ch_refs_sizes,medaka_models, ch_meta, rf_model)

        //mutated(ch_fqs2, ch_mut_refs, depths, models, filterModels, ch_fqsIllumina)

        //indels(ch_fqs2, ch_indel_refs, depths, models, indelFilterModels, ch_fqsIllumina)
}

