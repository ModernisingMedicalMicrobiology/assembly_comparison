#!/usr/bin/env python3
import pandas as pd
from argparse import ArgumentParser


def run(opts):
    df=pd.read_csv(opts.input)
    meta=pd.read_csv(opts.meta)
    
    ###### reduce to % #######
    g=df.groupby(['Sample name','Notes'])[['Gene found','name match']].sum().reset_index()
    print(g)
    
    g1=df.groupby(['Sample name','Notes'])[['Ref Gene name']].count().reset_index()
    g=g.merge(g1,on=['Sample name','Notes'],how='left')
    g['Percentage of genes found']=(g['Gene found']/g['Ref Gene name'])*100
    g['Percentage of names matching']=(g['name match']/g['Ref Gene name'])*100
    
    g['assembler']=opts.assembler
    g['assembly']=g['Sample name'] + '_' + g['assembler']
    g['subsample']=opts.subsample
    g.to_csv('prokka_genes_assembly_raw.csv')
    g=g.merge(meta, on='Sample name', how='left')
    print(g)
    #g=g.apply(getAssemblyDetails,axis=1)

    #g1=g[g['assembler'].isin(['flye','flye_hq', 'flye_medaka','flye_medakaX2','flye_medakaX3'])]
    #g1=g1[g1['run']==0]
    #g1=g1[g1['plexed']==False]
    #g2=g[g['assembler'].isin(['unicycler']) & g['Flow cell'].isin(['r9.4.1+Illumina'])]
    #g3=g[g['assembler'].isin(['spades']) & g['Flow cell'].isin(['Illumina'])]
    #g1=pd.concat([g1,g2,g3])
    g.to_csv(opts.output,index=False)



if __name__ == '__main__':
    parser = ArgumentParser(description='Parse dna diff and add meta data')
    parser.add_argument('-i', '--input', required=True,
                        help='input TSV file')
    parser.add_argument('-s', '--sample_name', required=True,
                        help='sample_name')
    parser.add_argument('-sub', '--subsample', required=True,
                        help='sub sampled reads')
    parser.add_argument('-a', '--assembler', required=True,
                        help='assembler')
    parser.add_argument('-o', '--output', required=True,
                        help='output tsv file')
    parser.add_argument('-m', '--meta', required=True,
                        help='meta data spreadsheet')

    opts, unknown_args = parser.parse_known_args()

    run(opts)