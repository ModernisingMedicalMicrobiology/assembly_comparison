#!/usr/bin/env python3
import pandas as pd
from argparse import ArgumentParser


def parse_file(f):
    df=pd.read_csv(f, sep='\t', usecols=[0,1,2,3,10,11],
                 names=['start','sub_ref', 'sub_q', 'q_pos', 'chrom','contig'])
    df['base change']=df['sub_ref'] + '->' + df['sub_q']
    g=df.groupby('base change')[['start']].count()
    return g

def run(opts):
    df=parse_file(opts.input)
    meta=pd.read_csv(opts.meta)
    df['Sample name']=opts.sample_name
    df['subsample']=opts.subsample
    df['assembler']=opts.assembler
    df=df.merge(meta, on='Sample name', how='left')
    #print(df)
    df.to_csv(opts.output)


if __name__ == '__main__':
    parser = ArgumentParser(description='Parse dna diff and add meta data')
    parser.add_argument('-i', '--input', required=True,
                        help='input TSV file')
    parser.add_argument('-s', '--sample_name', required=True,
                        help='sample_name')
    parser.add_argument('-sub', '--subsample', required=True,
                        help='sub sample')
    parser.add_argument('-a', '--assembler', required=True,
                        help='assembler')
    parser.add_argument('-o', '--output', required=True,
                        help='output tsv file')
    parser.add_argument('-m', '--meta', required=True,
                        help='meta data spreadsheet')

    opts, unknown_args = parser.parse_known_args()

    run(opts)
