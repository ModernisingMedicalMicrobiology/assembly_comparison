#!/usr/bin/env python3
import pandas as pd
from argparse import ArgumentParser


def run(opts):
    names=['start ref', 'end ref', 'start query', 'end query','aln len ref', 'aln len query',
            '% IDY', 'len ref', 'len query', 'cov ref', 'cov query', 'ref ID', 'query ID'
            ]
 
    df=pd.read_csv(opts.input,
                    sep='\t',
                    names=names)

    df['assembler']=opts.assembler
    df['Sample name']=opts.sample_name
    df['subsample']=opts.subsample
    df['Assembly']=df['Sample name'] + '_' + df['assembler']

    # ref % represented
    df['Total ref cov']=df.groupby(['Assembly','ref ID'])['cov ref'].transform(sum)

    # number of unique contigs
    df['No. unique contigs']=df.groupby(['Assembly','ref ID'])['query ID'].transform('nunique')

    # added meta data and save
    meta=pd.read_csv(opts.meta)
    df=df.merge(meta, on=['Sample name'], how='left')
    #df=df.apply(getAssemblyDetails,axis=1)
    

    # Plots and tables
    df2=df.drop_duplicates(subset=['Assembly','ref ID'])
    df2=df2[['Assembly','Species', 'Flow cell', 'model','BSA', 'basecaller',
            'assembler', 'subsample', 'plexed',
            'ref ID','len ref', 'Total ref cov', 'No. unique contigs']]
    df.to_csv('dnadiff_ref_recovery.csv',index=False)

    # Only Kleb plasmids
    df2=df2[df2['Species']=='K.pneumoniae']
    df2=df2[df2['ref ID']!='CP000647.1']
    df2.to_csv('dnadiff_plasmid_recovery.csv',index=False)

if __name__ == '__main__':
    parser = ArgumentParser(description='Parse dna diff and add meta data')
    parser.add_argument('-i', '--input', required=True,
                        help='input TSV file')
    parser.add_argument('-s', '--sample_name', required=True,
                        help='sample_name')
    parser.add_argument('-sub', '--subsample', required=True,
                        help='sub sampled reads')
    parser.add_argument('-a', '--assembler', required=True,
                        help='assembler')
    parser.add_argument('-m', '--meta', required=True,
                        help='meta data spreadsheet')

    opts, unknown_args = parser.parse_known_args()

    run(opts)