#!/usr/bin/env python3
import pandas as pd
from argparse import ArgumentParser

def run(opts):
    df=pd.read_csv(opts.input, sep='\t')
    df['Sample name']=opts.sample_name
    df['subsample']=opts.subsample

    meta=pd.read_csv(opts.meta)
    df=df.merge(meta, on='Sample name', how='left')

    df['Total bases']=df['sum_len']
    df['Total bases (MB)']=df['Total bases']/1000000
    df2=df[['Species','Sample name','subsample','Flow cell','basecaller', 'model','parent','plexed','run','BSA', 'num_seqs','Total bases', 'min_len', 'avg_len','max_len', 'N50','Q20(%)']]
    df2.to_csv(opts.output,index=False)

if __name__ == '__main__':
    parser = ArgumentParser(description='add features to data')
    parser.add_argument('-i', '--input', required=True,
                        help='input TSV file')
    parser.add_argument('-s', '--sample_name', required=True,
                        help='sample_name')
    parser.add_argument('-sub', '--subsample', required=True,
                        help='sub sample')
    parser.add_argument('-o', '--output', required=True,
                        help='output tsv file')
    parser.add_argument('-m', '--meta', required=True,
                        help='meta data spreadsheet')

    opts, unknown_args = parser.parse_known_args()

    run(opts)