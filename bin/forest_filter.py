#!/usr/bin/env python3
import pandas as pd
import numpy as np 
import pysam
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import pickle
from scipy.special import expit
from sklearn.ensemble import RandomForestClassifier
#from sklearn.utils.fixes import signature
from argparse import ArgumentParser
from collections import Counter

baseDict={'A':0, 'C':1, 'G':2, 'T':3, '.':4, 'N':5}


def bam_mix(bam):
    samfile = pysam.AlignmentFile(bam, "rb" )
    l=[]
    for pileupcolumn in samfile.pileup():
        chrom=pileupcolumn.reference_name
        pos=pileupcolumn.reference_pos
        no_reads=pileupcolumn.nsegments
        bases=pileupcolumn.get_query_sequences()
        bases=[b.upper() for b in bases if b.upper() in baseDict]
        baseCounts=Counter(bases)
        
        if len(baseCounts.most_common()) > 0:
            top_base=baseCounts.most_common()[0][1]
            majority_base_freq=top_base/no_reads
            top_base_seq=str(baseCounts.most_common()[0][0]).upper()
            top_base_int=baseDict[top_base_seq]
        else:
            top_base_seq='.'
            majority_base_freq=0
            top_base_int=baseDict[top_base_seq]
        if len(baseCounts.most_common()) > 1:
            next_top_base_seq=str(baseCounts.most_common()[1][0]).upper()     
        else:
            next_top_base_seq=top_base_seq

        next_top_base_int=baseDict[next_top_base_seq]
        l.append({'chrom':chrom, 'pos':pos, 'top_base_int':top_base_int, 
                  'majority base freq':majority_base_freq, 
                  'next_top_base_seq':next_top_base_seq, 'next_top_base_int':next_top_base_int})
    samfile.close()
    df=pd.DataFrame(l)
    return df

def getRefInts(ref):
    l=[]
    for seq in SeqIO.parse(open(ref,'rt'),'fasta'):
        chrom=seq.id
        for i,b in enumerate(seq.seq):
            l.append({'chrom':chrom, 'pos':i, 'ref':b, 'ref_int':baseDict[str(b).upper()]})
    df=pd.DataFrame(l)
    return df

def classify(df, modelFile):
    model = pickle.load(open(modelFile, 'rb'))
    features=['ref_int','top_base_int','majority base freq','next_top_base_int']
    X=np.array(df[features])

    # predict
    preds = model.predict(X)
    probs = model.predict_proba(X)

    # rearrange data
    probs=pd.DataFrame(probs,columns=[True,False])
    p=probs.max(axis=1)
    df['preds']=preds
    df['probs']=p
    return df

def make_output_fasta(df, out):
    df.sort_values(by=['chrom','pos'], inplace=True)
    seqs=[]
    for chrom in df['chrom'].unique():
        df2=df[df['chrom']==chrom]
        s=''.join(df2['new base'].to_list())
        seqrecord=SeqRecord(Seq(s), id=chrom, name='', description='RF filtered', dbxrefs=[])
        seqs.append(seqrecord)
    with open(out, 'wt') as outf:
        SeqIO.write(seqs, outf, 'fasta')
        

def run(bam, ref, out, model):
    df=bam_mix(bam)
    df2=getRefInts(ref)
    df=df.merge(df2, on=['chrom','pos'], how='left')
    df=classify(df, model)
    conditions  = [ (df['preds']==1) & (df['probs']>= 0.98), 
                   (df['preds']==1) & (df['probs']< 0.98),
                   df['preds']==0 ]
    choices     = [ 1, 0, 0 ]
    df['preds pass']=np.select(conditions, choices, default=0)
    df['preds pass']=np.where(df['majority base freq']<=0.7, df['preds pass'], 0)

    df['new base']=np.where(df['preds pass']==1, df['next_top_base_seq'], df['ref'])
    df2=df[df['preds pass']==1]
    df2.to_csv('changed_positions.csv')
    make_output_fasta(df, out)

if __name__ == '__main__':
    parser = ArgumentParser(description='Filter out mixed base changes in a bam file base on a RF model')
    parser.add_argument('-b', '--bam_file', required=True,
                        help='bam file input')
    parser.add_argument('-r', '--ref_file', required=True,
                        help='Reference file to change')
    parser.add_argument('-m', '--model', required=False, default=None,
                        help='model file for RF filter')
    parser.add_argument('-o', '--out_file', required=True,
                        help='output reference file after changes')
    opts, unknown_args = parser.parse_known_args()

    run(opts.bam_file, opts.ref_file, opts.out_file, opts.model)