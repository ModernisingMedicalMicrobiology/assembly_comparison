#!/usr/bin/env python3
import sys
from Bio import SeqIO, Seq
from Bio.Seq import Seq
import random
import pandas as pd
from argparse import ArgumentParser


SUBS=1000
DELS1=100
DELS2=100
DELS3=100
INS1=100
INS2=100
INS3=100

def makeSNP(inFasta, outFasta, mutsCSV, SUBS=1000):
    '''Make a fasta sequence with N random SNPs '''
    changes=[]
    bases=['A','C','G','T']
    outSeq=[]
    for seq in SeqIO.parse(open(inFasta, 'rt'), 'fasta'):
        subs=random.sample(range(0, len(seq.seq)), SUBS)
        subs.sort()
        newSeq=list(seq.seq)
        for sub in subs:
            base=newSeq[sub]
            BASES=bases.copy()
            if base in BASES: BASES.remove(base)
            change_base=random.choice(BASES)
            newSeq[sub]=change_base
            d={'pos':sub,
                    'Chrom':seq.id,
                    'ref':base,
                    'mut':change_base}
            changes.append(d)
        newSeq=''.join(newSeq)
        seq.seq=Seq( newSeq )
        outSeq.append(seq)
    
    df=pd.DataFrame(changes)
    print(df)
    df.to_csv(mutsCSV, index=False)
    
    with open(outFasta, 'wt') as outf:
        SeqIO.write(outSeq, outf,'fasta')

def makeINDEL(inFasta, outFasta, mutsCSV, indels=1000, maxIndel=10):
    '''Make a fasta sequence with N random Indels of random sizes to a max indel size'''
    changes=[]
    bases=['A','C','G','T']
    outSeq=[]
    for seq in SeqIO.parse(open(inFasta, 'rt'), 'fasta'):
        indel_pos=random.sample(range(0, len(seq.seq)), indels)
        indel_pos.sort()
        newSeq=list(seq.seq)
        mode='ins'
        for pos in indel_pos:
            if pos > len(newSeq): break
            base=newSeq[pos]
            indelSize=random.randint(1,maxIndel)
            if mode=='ins':
                insert_bases=random.choices(bases,k=indelSize)
                newSeq=newSeq[:pos] +  insert_bases + newSeq[pos:]
                change_base=''.join(insert_bases)
                d={'pos':pos,
                    'Chrom':seq.id,
                    'ref':base,
                    'mut':change_base,
                    'size':indelSize,
                    'Type':mode}
                mode='del'
                changes.append(d)
                continue
            if mode=='del':
                newSeq=newSeq[:pos] + newSeq[pos + indelSize:]
                change_base='-'
                d={'pos':pos,
                    'Chrom':seq.id,
                    'ref':base,
                    'mut':change_base,
                    'size':indelSize,
                    'Type':mode}
                mode='ins'
                changes.append(d)
                continue
        newSeq=''.join(newSeq)
        seq.seq=Seq( newSeq )
        outSeq.append(seq)
    
    df=pd.DataFrame(changes)
    print(df)
    df.to_csv(mutsCSV, index=False)
    
    with open(outFasta, 'wt') as outf:
        SeqIO.write(outSeq, outf,'fasta')


def run(args):
    if args.SNP==True:
        makeSNP(args.inFasta, args.outFasta, args.mutsCSV)
    elif args.INDEL==True:
        makeINDEL(args.inFasta, args.outFasta, args.mutsCSV)


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='mutate a reference to include SNPs or Indels')
    parser.add_argument('-i', '--inFasta', required=True,
            help='Input fasta sequence')
    parser.add_argument('-o', '--outFasta', required=True,
            help='output fasta sequence')
    parser.add_argument('-m', '--mutsCSV', required=True,
            help='mutations and positions in CSV format')
    parser.add_argument('-s', '--SNP', required=False, action='store_true',
            help='Add SNPs to sequence')
    parser.add_argument('-id', '--INDEL', required=False, action='store_true',
            help='Add INDELS to sequence')
    args = parser.parse_args()
    run(args)

