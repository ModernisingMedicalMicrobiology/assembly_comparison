#!/usr/usr/bin/env python3
import pandas as pd
import sys 
from Bio import SeqIO, pairwise2
from argparse import ArgumentParser

rcDict={'A':'T','a':'t',
        'T':'A','t':'a',
        'G':'C','g':'c',
        'C':'G','c':'g',
        'N':'N','n':'n',
        '-':'-','-':'-'}

class featureDetails:
    def __init__(self, gff_file, quast_file, ref_file, assembly_file,
            out_file):
        gff=self.loadGFF(gff_file)
        q=self.loadGenomicFeatures(quast_file)
        self.referenceSeqs=self.loadGenome(ref_file)
        self.assemblySeqs=self.loadGenome(assembly_file)
        self.out_file=out_file

        df=q.merge(gff, left_on='ID or #', right_on='ID', how='left')
        
        df=df.apply(self.getAssemblySequence, axis=1)
        self.df=df.apply(self.getReferenceSequence, axis=1)
        self.df['assembly gene sequence 10']=self.df['assembly gene sequence'].str.slice(0,10)
        self.df['ref gene sequence 10']=self.df['ref gene sequence'].str.slice(0,10)
        self.getAlignments()
        self.df=self.df[['Contig','ID','strand','start','end','Aln score',
            'assembly gene sequence 10','ref gene sequence 10', 
            'assembly gene aln',
            'ref gene aln']]
        print(self.df[['Contig','ID','strand','start','end','Aln score','assembly gene sequence 10','ref gene sequence 10']])
        self.saveFile()

    def getReferenceSequence(self, r):
        c=r['seqid']
        start=r['start']
        end=r['end']
        if r['strand']=='+':
            r['ref gene sequence']=str(self.referenceSeqs[c].seq[start-1:end])
        if r['strand']=='-':
            s=self.reverseComplement(self.referenceSeqs[c].seq[start-1:end])
            r['ref gene sequence']=s
        return r

    def getAssemblySequence(self, r):
        if r['Type']=='partial':
            r['assembly gene sequence']=None
            return r
        i=r['Contig'].split(':')
        c=i[0]
        i=i[1].split('-')
        start=int(i[0])
        end=int(i[-1])
        if start < end:
            s=str(self.assemblySeqs[c].seq[start-25:end+25])
        else:
            s=self.reverseComplement(str(self.assemblySeqs[c].seq[end-25:start+25]))
        if r['strand']=='+':
            r['assembly gene sequence']=s
        else:
            r['assembly gene sequence']=self.reverseComplement(s)
        return r

    def splitAttributes(self, r):
        for i in r['attributes'].split(';'):
            i=i.split('=')
            r[i[0]]=i[-1]
        return r
    
    
    def loadGFF(self, gff):
        df=pd.read_csv(gff, 
                sep='\t',
                comment='#',
                names=['seqid','source','type','start','end','score',
                    'strand','phase','attributes'])
        df=df[df['type']=='gene']
        df=df.apply(self.splitAttributes, axis=1)
        return df
    
    def loadGenomicFeatures(self, f):
        df=pd.read_csv(f,
                sep='\t',
                comment='=')#,
                #names=['ID', 'Start','End Type','Contig'])
        return df
    
    def loadGenome(self, f):
        d={}
        with open(f, 'rt') as inf:
            for seq in SeqIO.parse(inf, 'fasta'):
                d[seq.id]=seq
        return d

    def reverseComplement(self, s):
        rc=[]
        for b in reversed(s):
            try:
                rc.append(rcDict[b])
            except:
                rc.append(b)
        return ''.join(rc)

    def getAlignments(self):
        self.df=self.df.apply(self.alignSeqs,axis=1)

    def alignSeqs(self,r):
        s1=r['assembly gene sequence']
        s2=r['ref gene sequence']
        if s1==None or s2==None:
            r['Aln score']=None
            r['assembly gene aln']=None
            r['ref gene aln']=None
            return None
        alignments = pairwise2.align.globalxx(s1, s2)
        r['Aln score']=alignments[0].score
        r['assembly gene aln']=alignments[0].seqA
        r['ref gene aln']=alignments[0].seqB
        return r
    
    
    def saveFile(self):
        self.df.to_csv(self.out_file,index=False)


if __name__ == '__main__':
    parser = ArgumentParser(description='Get and compare features from ref and assembly using QUAST')
    parser.add_argument('-g', '--gff_file', required=True,
                        help='gff feature file')
    parser.add_argument('-r', '--ref_file', required=True,
                        help='reference sequence file')
    parser.add_argument('-a', '--assembly_file', required=True,
                        help='assembly sequence file')
    parser.add_argument('-q', '--quast_file', required=True,
                        help='quast genomic feature file')
    parser.add_argument('-o', '--out_file', required=True,
                        help='output tsv file')
    opts, unknown_args = parser.parse_known_args()
   
    featureDetails(opts.gff_file, 
            opts.quast_file, 
            opts.ref_file, 
            opts.assembly_file,
            opts.out_file)
