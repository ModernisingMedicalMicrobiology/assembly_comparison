#!/usr/bin/env python3
import pandas as pd
#import matplotlib.pyplot as plt
#import seaborn as sns


df=pd.read_csv('raw_sequencing_stats.csv')
df=df[df['subsample']=='full']
df['parent']=df['parent'].replace('Ecoli_BSA_r1041_simplex','Ecoli_BSA_r1041_hac')
df['basecaller + model']=df['basecaller'] + ' ' + df['model']

# bases

df2=df.pivot_table(index='parent', 
                   columns=['basecaller + model'], values='Total bases')

if 'Guppy DUPLEX' in df2.columns:
    df2['Guppy Duplex %']=(df2['Guppy DUPLEX']/(df2['Guppy SUP']))*100
if 'Dorado Duplex' in df2.columns:
    df2['Dorado Duplex %']=(df2['Dorado DUPLEX']/(df2['Dorado SUP']))*100

df2.dropna()
df2.to_csv('duplex_bases_stats.csv')

# bar plot
# df4=df2.reset_index()
# df3=pd.melt(df4, id_vars=['parent'],
#             value_vars=['Dorado DUPLEX', 'Dorado SUP', 'Guppy DUPLEX', 'Guppy HAC',   'Guppy SUP'], 
#             var_name='Basecaller + model',
#             value_name='Total bases')

# df3.dropna(inplace=True)
# ax1=sns.barplot(y='parent', x='Total bases', hue='Basecaller + model', data=df3)
# ax1.set(xscale="log")
# sns.move_legend(ax1, "upper left", bbox_to_anchor=(1, 1))
# plt.tight_layout()
# plt.savefig('total_bases_bar.pdf')



# reads
df2=df.pivot_table(index='parent', 
                   columns=['basecaller + model'], values='num_seqs')


if 'Guppy DUPLEX' in df2.columns:
    df2['Guppy Duplex %']=(df2['Guppy DUPLEX']/(df2['Guppy SUP']))*100
if 'Dorado Duplex' in df2.columns:
    df2['Dorado Duplex %']=(df2['Dorado DUPLEX']/(df2['Dorado SUP']))*100

df2.dropna()
df2.to_csv('duplex_reads_stats.csv')



# bar plot
# df4=df2.reset_index()
# df3=pd.melt(df4, id_vars=['parent'],
#             value_vars=['Dorado DUPLEX', 'Dorado SUP', 'Guppy DUPLEX', 'Guppy HAC',   'Guppy SUP'], 
#             var_name='Basecaller + model',
#             value_name='Total reads')

# df3.dropna(inplace=True)
# ax1=sns.barplot(y='parent', x='Total reads', hue='Basecaller + model', data=df3)
# ax1.set(xscale="log")
# sns.move_legend(ax1, "upper left", bbox_to_anchor=(1, 1))
# plt.tight_layout()
# plt.savefig('total_reads_bar.pdf')


