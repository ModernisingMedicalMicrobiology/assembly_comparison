#!/usr/bin/env python3
import pandas as pd
import numpy as np
from argparse import ArgumentParser


def run(opts):
    df=pd.read_csv(opts.input)
    if len(df)>1000:
        df=df.sample(1000)
    meta=pd.read_csv(opts.meta)

    df['Sample name']=opts.sample_name
    df['Subsample']=opts.subsample
    df=df.merge(meta,on='Sample name', how='left')
    df['Probability of incorrect base call']=df['NM']/df['query_length']
    df['log']=np.log10(df['Probability of incorrect base call'])
    df['PHRED score']=-10*df['log']
    df['PHRED score non-indels']=-10*np.log10(df['Subs']/df['query_length'])
    df.to_csv(opts.output, index=False)

if __name__ == '__main__':
    parser = ArgumentParser(description='Process raw read stats')
    parser.add_argument('-i', '--input', required=True,
                        help='input csv file')
    parser.add_argument('-s', '--sample_name', required=True,
                        help='sample_name')
    parser.add_argument('-sub', '--subsample', required=True,
                        help='sub sampled reads')
    parser.add_argument('-o', '--output', required=True,
                        help='output tsv file')
    parser.add_argument('-m', '--meta', required=True,
                        help='meta data spreadsheet')

    opts, unknown_args = parser.parse_known_args()

    run(opts)
