#!/usr/bin/env python3
import sys
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_style('darkgrid')

resultsFol=sys.argv[1]

def raw_reads_stats(resultsFol):
    df=pd.read_csv('{0}/raw_read_stats.csv'.format(resultsFol))
    df=df[df['Subsample']==0]
      
    # plots  
    df.sort_values(by=['Sample name'], inplace=True)
    ax1=sns.boxplot(y='Sample name', x='% match',
            hue='Flow cell',
            data=df)
    #ax1.set( yscale="log")
    plt.xlim(90,101)
    plt.savefig('figs/Percent_match.png')
    plt.savefig('figs/Percent_match.pdf')
    plt.savefig('figs/Percent_match.svg')
    #plt.show()
    plt.clf()
    
    ax3=sns.boxplot(y='Sample name', x='PHRED score',
            hue='Flow cell',
            data=df)
    #ax1.set( yscale="log")
    plt.savefig('figs/PHRED.png')
    plt.savefig('figs/PHRED.pdf')
    plt.clf()
    
    ax4=sns.boxplot(y='Sample name', x='PHRED score non-indels',
            hue='Flow cell',
            data=df)
    #ax1.set( yscale="log")
    plt.savefig('figs/PHRED_subs_only.pdf')
    plt.savefig('figs/PHRED_subs_only.png')
    plt.clf()

def raw_sequencing_stats(resultsFol):
    df=pd.read_csv('{0}/raw_sequencing_stats.csv'.format(resultsFol))
    df=df[df['subsample']==0]

    df['Total bases (MB)']=df['Total bases']/1000000
    ax1=sns.barplot(x='Total bases (MB)',y='Sample name',
            hue='Flow cell',
            data=df)
    #ax1.set(xscale="log")
    #plt.tight_layout()
    plt.savefig('figs/Total_bases_bar.svg')
    plt.savefig('figs/Total_bases_bar.png')
    plt.savefig('figs/Total_bases_bar.pdf')
    #plt.show()
    plt.clf()

def prokkaGenes(resultsFol):
    g1=pd.read_csv('{0}/genes_found_flowcell.csv'.format(resultsFol))
    g1['FC+Model']=g1['Flow cell'] + ' ' + g1['model'] + ' ' + g1['basecaller']
    g1=g1[g1['BSA']==True]

    #g2['FC+Model']=g2['Flow cell'] + ' ' + g2['model']
    
    #ax2=sns.barplot(x='Species', y='Percentage of genes found',
    #        hue='FC+Model',data=g2,
    #        )
    #plt.legend(loc='lower right')
    #ax2.set_ylim(80,100)
    #plt.tight_layout()
    #plt.savefig('figs/genes_found_flowcell.png')
    #plt.savefig('figs/genes_found_flowcell.pdf')
    #plt.savefig('figs/genes_found_flowcell.svg')
    #plt.show()
    #plt.clf()
    
    #ax2=sns.barplot(x='Species', y='Percentage of names matching',
    #        hue='FC+Model',data=g2,
    #        )
    #ax2.set_ylim(50,100)
    #plt.legend(loc='lower right')
    #plt.tight_layout()
    #plt.savefig('figs/names_matches_flowcell.pdf')
    #plt.savefig('figs/names_matches_flowcell.png')
    #plt.savefig('figs/genes_matches_flowcell.svg')
    #plt.show()
    #plt.clf()

    ax3=sns.catplot(x='subsample', y='Percentage of genes found',
            hue='FC+Model',data=g1,
            kind='bar',
            col='Species',
            row='assembler',
            )
    axes = ax3.axes
    #ax3.set_ylim(90,100)
    axes[0,1].set_ylim(90,100)
    plt.savefig('figs/genes_found_flowcell_subsample.png')
    plt.savefig('figs/genes_found_flowcell_subsample.pdf')
    plt.savefig('figs/genes_found_flowcell_subsample.svg')
    #plt.show()
    plt.clf()

def get_assembly_DNAdiff_stats(resultsFol):
    df=pd.read_csv('{0}/dnadiff.csv'.format(resultsFol))
    df['Assembly']=df['Sample name'] + '_' + df['assembler']
    df['FC+Model']=df['Flow cell'] + ' ' + df['model']
    #df=df.apply(getAssemblyDetails,axis=1)
    df['Number']=df['Query']
    #df=df.apply(filterSUPspades,axis=1)
    #df=df[df['SUP filter']==False]
    ## plots
    hue_order=['r9.4.1 HAC', 'r9.4.1 SUP',
            'r10.3 Rerio','r10.3 SUP',
            'r10.4 HAC', 'r10.4 SUP','r10.4 DUPLEX',
            'r10.4.1 SUP','r10.4.1 DUPLEX',
            'r9.4.1+Illumina', 'Illumina']

    #df2=df[df['run']==0]
    df2=df[df['subsample']==100]
    #df2=df2[df2['plexed']==False]
    #print(df2)
 
    ax1=sns.catplot(x='assembler', y='Number', data=df2, hue='FC+Model',
            kind='bar',
            col='Species',row='type',
            log=True)
    plt.savefig('figs/Assembly_dnadiff.png')
    plt.savefig('figs/Assembly_dnadiff.pdf')
    plt.savefig('figs/Assembly_dnadiff.svg')
    #plt.show()
    plt.clf()

    # per subsample
    hue_order=['r9.4.1 HAC', 'r9.4.1 SUP',
            'r10.3 Rerio','r10.3 SUP',
            'r10.4 HAC', 'r10.4 SUP','r10.4 DUPLEX',
            'r10.4.1 SUP','r10.4.1 DUPLEX',
            'r9.4.1+Illumina']
    df3=df[df['plexed']==False]
    #df3=df3[df3['assembler'].isin(['flye_medaka','unicycler'])]
    ax1=sns.catplot(x='subsample', y='Number', data=df3, hue='FC+Model',
            kind='bar',
            col='Species',row='type',
            log=True)
    plt.savefig('figs/Assembly_dnadiff_subsample.png')
    plt.savefig('figs/Assembly_dnadiff_subsample.pdf')
    plt.savefig('figs/Assembly_dnadiff_subsample.svg')
    #plt.show()
    plt.clf()


raw_reads_stats(resultsFol)

raw_sequencing_stats(resultsFol)

prokkaGenes(resultsFol)

get_assembly_DNAdiff_stats(resultsFol)