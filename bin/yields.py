#!/usr/bin/env python3
import pandas as pd
import numpy as np
from argparse import ArgumentParser
from Bio import SeqIO
import gzip

def getTimes(fq):
    l=[]
    fq_name=fq.split('/')[-1].replace('.fastq.gz','')
    with gzip.open(fq,'rt') as handle:
        for seq in SeqIO.parse(handle, 'fastq'):
            desc={i.split('=')[0]:i.split('=')[-1] for i in seq.description.split(' ')}
            d={'id':seq.id,
                'time':desc['start_time'],
                'seq_len':len(seq.seq),
                'channel':desc['ch'],
                'read':desc['read'],
                'runid':desc['runid'],
                'name':fq_name}
            l.append(d)
    df=pd.DataFrame(l)
    df['start time']=pd.to_datetime(df['time'])
    start_time=df['start time'].min()
    df['run time']=df['start time']-start_time
    #df.to_csv('{0}.csv'.format(fq_name),index=False)
    return df

def sampleDF(df):
    df['run time']=pd.to_timedelta(df['run time'])
    df['seconds']=df['run time'].dt.total_seconds()
    df['Hours']=df['seconds']/3600
    df=df.sort_values(by=['seconds'])
    df['cumulative bases']=df['seq_len'].cumsum()
    if len(df) > 1000:
        df2=df.sample(1000)
    else:
        df2=df
    df2=df2.sort_values(by=['seconds'],)
    #df2.to_csv('runtimes_sampled/{}'.format(csv),index=False)
    return df2

def run(opts):
    df=getTimes(opts.fastq)
    df=sampleDF(df)
    df=df.sort_values(by=['name','seconds'])

    meta=pd.read_csv(opts.meta)
    df['Sample name']=opts.sample_name
    df=df.merge(meta, on='Sample name')
    df=df.sort_values(by=['name','seconds'])
    df.to_csv(opts.output,index=False)



if __name__ == '__main__':
    parser = ArgumentParser(description='Get read times to generate yield plots')
    parser.add_argument('-f', '--fastq', required=True,
                        help='input csv file')
    parser.add_argument('-s', '--sample_name', required=True,
                        help='sample_name')
    parser.add_argument('-o', '--output', required=True,
                        help='output tsv file')
    parser.add_argument('-m', '--meta', required=True,
                        help='meta data spreadsheet')

    opts, unknown_args = parser.parse_known_args()

    run(opts)