import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_style('darkgrid')

df=pd.read_csv('data.csv')

g=df.groupby(['Sample name','Notes'])[['Gene found','Sequence match']].sum().reset_index()
print(g)

g1=df.groupby(['Sample name','Notes'])[['Gene name']].count().reset_index()
g=g.merge(g1,on=['Sample name','Notes'],how='left')
g['Percentage of genes found']=(g['Gene found']/g['Gene name'])*100
g['Percentage of genes matching']=(g['Sequence match']/g['Gene name'])*100

flowcells={'r10':'r10.3',
        'r10.4':'r10.4',
        'r9':'r9.3'}

hue_order=['r9.3', 'r10.3', 'r10.4', 'r9.3+Illumina']

def get_flowcell(r):
    flowcell=r.split('_')[-1]
    return flowcells[flowcell]


bugs={'Ecoli':'E.coli',
        'Kpneumo':'K.pneumoniae',
        'MRSA':'S.aureus',
        'Pa01':'P.aeruginosa'}

def getBug(r):
    bug=r.split('_')[0]
    return bugs[bug]


g['flowcell']=g['Sample name'].map(get_flowcell)
g['species']=g['Sample name'].map(getBug)
print(g)



#plots
ax=sns.barplot(y='Sample name',x='Percentage of genes found',
        hue='Notes',data=g, ci=0)
ax.set_xlim(80,100)
plt.tight_layout()
#plt.show()
plt.savefig('figs/genes_found.png')
plt.savefig('figs/genes_found.pdf')
plt.savefig('figs/genes_found.svg')
plt.clf()


ax1=sns.barplot(y='Sample name',x='Percentage of genes matching',
        hue='Notes',data=g, ci=0)
ax1.set_xlim(50,100)
plt.tight_layout()
#plt.show()
plt.savefig('figs/genes_matches.png')
plt.savefig('figs/genes_matches.pdf')
plt.savefig('figs/genes_matches.svg')
plt.clf()


g1=g[g['Notes'].isin(['flye'])]
g2=g[g['Notes'].isin(['unicycler']) & g['flowcell'].isin(['r9.3'])]
print(g2)
g2['flowcell']='r9.3+Illumina'
g1=pd.concat([g1,g2])

ax2=sns.barplot(x='species', y='Percentage of genes found',
        hue='flowcell',data=g1,
        hue_order=hue_order)
plt.legend(loc='lower right')
ax2.set_ylim(90,100)
plt.tight_layout()
plt.savefig('figs/genes_found_flowcell.png')
plt.savefig('figs/genes_found_flowcell.pdf')
plt.savefig('figs/genes_found_flowcell.svg')
plt.show()
plt.clf()

ax2=sns.barplot(x='species', y='Percentage of genes matching',
        hue='flowcell',data=g1,
        hue_order=hue_order)
ax2.set_ylim(50,100)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('figs/genes_matches_flowcell.png')
plt.savefig('figs/genes_matches_flowcell.pdf')
plt.savefig('figs/genes_matches_flowcell.svg')
plt.show()
plt.clf()
