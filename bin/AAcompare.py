#!/usr/bin/env python3
import pandas as pd
import sys 
from Bio import SeqIO, pairwise2
from argparse import ArgumentParser

rcDict={'A':'T','a':'t',
        'T':'A','t':'a',
        'G':'C','g':'c',
        'C':'G','c':'g',
        'N':'N','n':'n',
        '-':'-','-':'-'}

class compareGenbank:
    def __init__(self, ref_gff_file, test_gff_file, 
            out_file, sampleName, notes=None):
        self.out_file=out_file
        self.notes=notes
        self.sampleName=sampleName
        self.ref_genes=self.loadGenes(ref_gff_file)
        self.test_genes=self.loadGenes(test_gff_file)

        self.compareGenes()
        self.saveFile()


    def compareGenes(self):
        l=[]
        for gene in self.ref_genes:
            ref_gene_len=len(gene)
            refAA=gene
            refName=self.ref_genes[gene]
            if gene in self.test_genes:
                g=1
                test_gene_len=len(gene)
                testAA=gene
                testName=self.test_genes[gene]
                if self.ref_genes[gene] == self.test_genes[gene]:
                    m=1
                else:
                    m=0
            else:
                g=0
                m=0
                test_gene_len=0
                testName=None
                testAA=None
            l.append({'Sample name':self.sampleName,
                'Ref Gene name':refName,
                'test Gene name':testName,
                'name match':m,
                'Gene found':g,
                'Ref gene length (AA)':ref_gene_len,
                'Test gene length (AA)':test_gene_len})
                #'Ref AA':refAA,
                #'Test AA':testAA})
        self.df=pd.DataFrame(l)
        self.df['Notes']=self.notes


    def loadGenes(self,gbk):
        d={}
        for seq_record in SeqIO.parse(gbk, "genbank"):
            for seq_feature in seq_record.features:
                if seq_feature.type == "CDS":
                    if 'gene' in seq_feature.qualifiers:
                        #d[seq_feature.qualifiers['gene'][0]]=seq_feature.qualifiers['translation'][0]
                        d[seq_feature.qualifiers['translation'][0]]=seq_feature.qualifiers['gene'][0]
                    else:
                        try:
                            d[seq_feature.qualifiers['translation'][0]]=seq_feature.qualifiers['locus_tag'][0]
                        except:                
                            pass
        return d
    
    
    def saveFile(self):
        self.df.to_csv(self.out_file,index=False)


if __name__ == '__main__':
    parser = ArgumentParser(description='Get and compare features from ref and assembly using Genbank files')
    parser.add_argument('-rg', '--ref_gff_file', required=True,
                        help='reference gff feature file')
    parser.add_argument('-tg', '--test_gff_file', required=True,
                        help='test gff feature file')
    parser.add_argument('-n', '--notes', required=False, default=None,
                        help='optional comment or note for each line')
    parser.add_argument('-s', '--sampleName', required=False, default=None,
                        help='sample name')
    parser.add_argument('-o', '--out_file', required=True,
                        help='output tsv file')
    opts, unknown_args = parser.parse_known_args()
   
    compareGenbank(opts.ref_gff_file, 
            opts.test_gff_file, 
            opts.out_file,
            opts.sampleName,
            notes=opts.notes)
