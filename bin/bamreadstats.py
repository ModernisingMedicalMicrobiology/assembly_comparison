#!/usr/bin/env python3
import pysam
import sys
import pandas as pd

n=0
samfile = pysam.AlignmentFile(sys.argv[1], "rb")

reads=[]
for read in samfile.fetch():
    #if n > 100000: break
    cig=read.get_cigar_stats()
    try:
        NM=read.get_tag('NM')
    except:
        NM=0
    d={'Insertions':cig[0][1],
            'Deletions':cig[0][2],
            'Matches':cig[0][0],
            'MAPQ':read.mapping_quality,
            'NM':NM,
            'query_length':read.query_alignment_length}
    reads.append(d)
    n+=1

df=pd.DataFrame(reads)
df['Sample name']=sys.argv[2]
df['% match']=100*((df['query_length']-df['NM'])/df['query_length'])
df['Subs']=df['NM']-(df['Insertions']+df['Deletions'])
df['% ins']=100*(df['Insertions']/df['query_length'])
df['% del']=100*(df['Deletions']/df['query_length'])
print(df)
print(df.describe())
df.to_csv('{0}.read_stats.csv'.format(sys.argv[2]), index=False)
