# assembly_comparison

This workflow has a number of componants:
- Create assemblies and compares again reference sequences using blast. 
- Map to reference sequences and calculate raw reads accuracies.

## Running the workflow

The command to run the workflow are as such, this is using singularity, but docker and conda are also available.

```bash
nextflow https://gitlab.com/ModernisingMedicalMicrobiology/assembly_comparison \
                -profile singularity \
                --inputFastq infastq.csv \
                --inputIlluminaFastq inIlluminaFastqs.csv \
                -with-trace \
                -resume
```

The input csv examples can be seen in `inputCSVs`:
