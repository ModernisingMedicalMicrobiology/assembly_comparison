# Use an official Python runtime as a parent image
FROM ubuntu:20.04
FROM continuumio/miniconda3
RUN conda config --add channels defaults && \
    conda config --add channels bioconda && \
    conda config --add channels conda-forge

#COPY medaka_env.yml /
RUN conda install -c conda-forge mamba
RUN mamba create -n medaka -c conda-forge -c bioconda medaka  && \
	conda init bash

ENV PATH /opt/conda/envs/medaka/bin:$PATH

RUN apt-get --allow-releaseinfo-change update && apt-get install -y procps
