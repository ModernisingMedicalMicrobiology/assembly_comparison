#!/bin/bash

bugs="Ecoli
Kpneumo
MRSA
Pa01"

for bug in ${bugs}
do
    python3 ../bin/mutateRef.py \
            -i ${bug}.fasta \
            -o INDELs/${bug}_mut.fasta \
            -m INDELs/${bug}_muts.csv \
	    -id
done
