#!/bin/bash

bugs="Ecoli
Kpneumo
MRSA
Pa01"

for bug in ${bugs}
do
    python3 ../bin/mutateRef.py \
            -i ${bug}.fasta \
            -o SNPs/${bug}_mut.fasta \
            -m SNP_CSVs/${bug}_muts.csv \
	    -s
done
