bugs="Ecoli
Kpneumo
MRSA252
Pa01"

for bug in ${bugs}
do
    python3 ~/soft/assembly_comparison/bin/mutateRef.py \
	    ${bug}.fasta \
	    ${bug}_mut.fasta \
	    ${bug}_muts.csv
done

