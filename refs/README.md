# Generate mutated reference sequences for SNP and INDEL detection 

These commands generate random SNP and INDELS into reference sequences. Then reads can be mapped and limits of detection calculated.

## Generate SNP mutations

An example command will generate 1000 substitutions in a reference sequence

```bash
python3 ../bin/mutateRef.py \
    -i MRSA.fasta \
    -o MRSA_mut.fasta \
    -m MRSA_muts.csv \
    -s
```
