
#!/usr/bin/env nextflow

// enable dsl2
nextflow.preview.dsl = 2

// include modules
include {filter} from './modules/main_procs.nf'
include {assemble} from './modules/main_procs.nf'
include {makeblastdb} from './modules/main_procs.nf'
include {blast} from './modules/main_procs.nf'
include {minimap2} from './modules/main_procs.nf'
include {mapstats} from './modules/main_procs.nf'




workflow {
    main:

    filter(ch_fqs)

    assemble(filter.out)

    makeblastdb(ch_refs)

    blast(assemble.out.combine(makeblastdb.out, by:1))

    minimap2(ch_fqs2.combine(ch_refs, by:0))

    mapstats(minimap2.out)
}
