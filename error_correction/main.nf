#!/usr/bin/env nextflow

// enable dsl2
nextflow.enable.dsl=2

// include modules
include {MAP} from './modules/main_procs.nf'
include {INDEX_BAM} from './modules/main_procs.nf'
include {NAMESORT_BAM} from './modules/main_procs.nf'
include {POS_SORT_BAM} from './modules/main_procs.nf'
include {REPAIR_BAM} from './modules/main_procs.nf'
include {MODKIT} from './modules/main_procs.nf'
include {CLAIR3} from './modules/main_procs.nf'
include {PYSAMSTATS} from './modules/main_procs.nf'
include {SNP_CANDIDATES} from './modules/main_procs.nf'


// main workflow
workflow {
    Channel.fromPath( "${params.inputFastq}/*" )
        .map{ file -> tuple(file.simpleName, file) }
        .set{ ch_fqs }
        
    Channel.fromPath( "${params.ref}" )
        .set{ ref }

    Channel.fromPath( "${params.model}" )
        .set{ model }

    Channel.fromPath( "${params.modbam}" )
        .set{ modbam }

    main:
    MAP(ch_fqs, ref)

    INDEX_BAM(MAP.out.bam, ref)

    NAMESORT_BAM(MAP.out.bam)

    REPAIR_BAM(NAMESORT_BAM.out.combine(modbam))

    POS_SORT_BAM(REPAIR_BAM.out.bam)

    MODKIT(POS_SORT_BAM.out)

    //CLAIR3(INDEX_BAM.out, model)

    PYSAMSTATS(INDEX_BAM.out)

    SNP_CANDIDATES(PYSAMSTATS.out.tab)
}