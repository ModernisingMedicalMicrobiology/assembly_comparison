process MAP {
    tag {sample}
    label 'map'


    cpus 4

    input:
	tuple val(sample), file('reads.fastq')
    each path('ref.fa')

    output:
	tuple val(sample), file("${sample}.sorted.bam"), emit: bam

    script:
    """
    minimap2 -t ${task.cpus} -ax map-ont ref.fa 'reads.fastq' |\
        samtools view -b - | samtools sort -o ${sample}.sorted.bam 
    """
    stub:
    """
    touch "${sample}.sorted.bam"
    """
}

process INDEX_BAM {
    tag {sample}
    label 'map'
    cpus=1
    publishDir "bams/${task.process.replaceAll(":","_")}", pattern:'*.bam' , saveAs: { filename -> "${sample}.bam"} 
    publishDir "bams/${task.process.replaceAll(":","_")}", pattern:'*.bam.bai' , saveAs: { filename -> "${sample}.bam.bai"} 
    publishDir "bams/${task.process.replaceAll(":","_")}", pattern:'*.fa' , saveAs: { filename -> "${sample}.fasta"} 
    publishDir "bams/${task.process.replaceAll(":","_")}", pattern:'*.fai' , saveAs: { filename -> "${sample}.fasta.fai"} 


    input:
    tuple val(sample), path('mapped.bam')
    each path('ref.fa')
    
    output:
    tuple val(sample), path('mapped.bam'), path('mapped.bam.bai'), path('ref.fa'), path('ref.fa.fai')

    script:
    """
    samtools index mapped.bam
    samtools faidx ref.fa
    """
}

process NAMESORT_BAM {
    tag {sample}
    label 'map'
    cpus=1

    input:
    tuple val(sample), path('mapped.bam')
    
    output:
    tuple val(sample), path("${sample}.bam")

    script:
    """
    samtools sort -n -o ${sample}.bam mapped.bam
    """
}

process REPAIR_BAM {
    tag {sample}
    label 'map'
    cpus=4
    publishDir "logs/${task.process.replaceAll(":","_")}", pattern:'*.log'

    input:
    tuple val(sample), path("${sample}.aln.bam"), path("${sample}.mod.bam")
    
    output:
    tuple val(sample), path("${sample}.bam"), emit: bam
    tuple val(sample), path("${sample}.log"), emit: log


    script:
    """
    /mnt/data/soft/modkit/dist/modkit repair \
        -d ${sample}.mod.bam \
        -a ${sample}.aln.bam \
        -o ${sample}.bam \
        --log-filepath ${sample}.log \
        -t ${task.cpus}
    """
}

process POS_SORT_BAM {
    tag {sample}
    label 'map'
    cpus=1

    input:
    tuple val(sample), path('mapped.bam')
    
    output:
    tuple val(sample), path("${sample}.bam"), path("${sample}.bam.bai")

    script:
    """
    samtools sort -o ${sample}.bam mapped.bam
    samtools index ${sample}.bam
    """
}

process MODKIT {
    tag {sample}
    label 'map'

    cpus 4

    publishDir "beds/${task.process.replaceAll(":","_")}"

    input:
    tuple val(sample), path("${sample}.bam"), path("${sample}.bam.bai")

    output:
    //tuple val(sample), path("${sample}.bed"), emit: bed
    tuple val(sample), path("${sample}.bedgraph"), emit: bedgraph

    script:
    """
    /mnt/data/soft/modkit/dist/modkit pileup \
        ${sample}.bam \
        --bedgraph ${sample}.bedgraph \
        -t ${task.cpus} 

    """
    stub:
    """
    touch ${sample}.bed
    """
}

process CLAIR3 {
    tag {sample}
    label 'clair3'
    cpus=1

    publishDir "VCFs/${task.process.replaceAll(":","_")}", mode: 'copy', saveAs: { filename -> "${sample}.vcf.gz"} 


    input:
    tuple val(sample), path('mapped.bam'), path('mapped.bam.bai'), path('ref.fa'), path('ref.fa.fai')
    each path('model')

    output:
    tuple val(sample), path("clair_out/merge_output.vcf.gz"), emit: vcf

    script:
    """
    run_clair3.sh \
        --bam_fn=mapped.bam \
        --ref_fn=ref.fa \
        --threads=${task.cpus} \
        --platform="ont" \
        --model_path=model \
        --include_all_ctgs \
        --no_phasing_for_fa \
        --output=clair_out

    """
}

process PYSAMSTATS {
    tag {sample}
    label 'pysamstats'
    cpus=1

    publishDir "tab/${task.process.replaceAll(":","_")}" 


    input:
    tuple val(sample), path('mapped.bam'), path('mapped.bam.bai'), path('ref.fa'), path('ref.fa.fai')

    output:
    tuple val(sample), path("${sample}.tab.gz"), emit: tab

    script:
    """
    pysamstats -t variation_strand mapped.bam -f ref.fa | gzip > ${sample}.tab.gz
    """
}

process SNP_CANDIDATES {
    tag {sample}
    label 'map'

    publishDir "tab/${task.process.replaceAll(":","_")}" 

    input:
    tuple val(sample), path("${sample}.tab.gz")

    output:
    tuple val(sample), path("${sample}.tsv")

    script:
    """
    snp_candidates.py -i "${sample}.tab.gz" -o ${sample}.tsv
    """
}