#!/usr/bin/env python3
import pandas as pd
from argparse import ArgumentParser

def run(opts):
    df=pd.read_csv(opts.input, sep='\t',usecols=['chrom','pos','ref','reads_all','A','T','C','G'])
    df['top_base'] = df[['A','T','C','G']].max(axis=1)
    df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
    df['majority base freq'] = (df['top_base'] / df['reads_all'])
    df=df[df['majority base freq']<=float(opts.freq)]

    df.to_csv(opts.output, index=False, sep='\t')

if __name__ == '__main__':
    parser = ArgumentParser(description='Find SNP candidates based on base mix at all positions')
    parser.add_argument('-i', '--input', required=True,
                        help='input pysamstats TSV file')
    parser.add_argument('-f', '--freq', required=False, default=0.8,
                        help='frequency of dominant base to consider')
    parser.add_argument('-o', '--output', required=True,
                        help='output reduced pysamstats TSV file')

    opts, unknown_args = parser.parse_known_args()

    run(opts)