#!/usr/bin/env nextflow

// enable dsl2
nextflow.enable.dsl=2

//params 
params.cudadevice='auto'
params.doradocudadevice='cuda:all'
params.methylation_model='dna_r10.4_e8.1_modbases_5hmc_5mc_cg_hac.cfg'

// include modules
//dorado
include {simplex_basecall} from './modules/procs.nf'
include {simplex_basecall as simplex_basecall_SUP} from './modules/procs.nf'
include {merge_dorado_bams} from './modules/procs.nf'
include {download_models} from './modules/procs.nf'
include {find_pairs} from './modules/procs.nf'
include {find_additional_pairs} from './modules/procs.nf'
include {stereo_basecall} from './modules/procs.nf'
include {stereo_basecall_additional} from './modules/procs.nf'
include {modkit_dorado} from './modules/procs.nf'

// guppy 
include {guppy_fast} from './modules/procs.nf'
include {guppy_methylation} from './modules/procs.nf'
include {merge_bams} from './modules/procs.nf'
include {modbam2bed} from './modules/procs.nf'
include {modkit} from './modules/procs.nf'
include {find_pairs_guppy} from './modules/procs.nf'
include {filter_pairs} from './modules/procs.nf'
include {guppy_duplex} from './modules/procs.nf'
include {cat_fqs} from './modules/procs.nf'
include {samToBamFastq as samToBamFastqFast} from './modules/procs.nf'
include {samToBamFastq as samToBamFastqDuplex} from './modules/procs.nf'
include {samToBamFastq as samToBamFastqSUP} from './modules/procs.nf'
include {f5ToPod5} from './modules/procs.nf'



workflow dorado {
    take:
    ch_f5s

    main:
    download_models()

    f5ToPod5(ch_f5s)

    //simplex_basecall(f5ToPod5.out.combine(download_models.out.fast))

    simplex_basecall_SUP(f5ToPod5.out.combine(download_models.out.sup).combine(download_models.out.sup_6mA))

    groupedBams=simplex_basecall_SUP.out.bam.groupTuple()

    merge_dorado_bams(groupedBams)

    //modkit_dorado(merge_dorado_bams.out.bam)

    //find_pairs(simplex_basecall.out)

    //find_additional_pairs(simplex_basecall.out.combine(ch_f5s, by:[0,1]))

    //stereo_basecall(f5ToPod5.out.combine(download_models.out.sup)
    //    .combine(find_pairs.out,by:[0,1])
    //    .combine(download_models.out.stereo))

    //stereo_basecall_additional(ch_f5s.combine(download_models.out.sup)
    //    .combine(find_additional_pairs.out,by:[0,1])
    //    .combine(download_models.out.stereo))

    //samToBamFastqFast(simplex_basecall.out)
    samToBamFastqSUP(simplex_basecall_SUP.out.fileNameBam)
    //samToBamFastqDuplex(stereo_basecall.out)
}

workflow guppy {
    take:
    ch_f5s
    refs

    main:
    guppy_fast(ch_f5s)

    guppy_methylation(ch_f5s.combine(refs, by:0))

    merge_bams(guppy_methylation.out.fol)

    Channel.from('modA', 'modC', 'modG', 'modT', 
        '5mC', '5hmC', '5fC', '5caC', '5hmU',
        '5fU', '5caU', '6mA', '5oxoG', 'Xao')
        .set{mod}

    //modbam2bed(merge_bams.out.bam.combine(refs, by: 0).combine(mod))

    modkit(merge_bams.out.bam.combine(refs, by: 0))

    find_pairs_guppy(guppy_fast.out)

    filter_pairs(guppy_fast.out.combine(find_pairs_guppy.out, by:0))

    guppy_duplex(ch_f5s.combine(find_pairs_guppy.out, by:0)
        .combine(filter_pairs.out, by:0))

    guppy_duplex_cat=guppy_duplex.out
        .map{row -> tuple(row[0], row[1], 'duplex')}

    guppy_fast_cat=guppy_fast.out
        .map{row -> tuple(row[0], row[1], 'sup')}

    cat_ch=guppy_fast_cat.mix(guppy_duplex_cat)

    cat_fqs(cat_ch)
}

workflow {
    // channels
    Channel.fromPath( "${params.inputFast5s}" )
           .splitCsv()
           .map { row -> tuple(row[0], file(row[1])) }
           .set{ ch_f5s }

    Channel.fromPath( "${params.refs}" )
           .splitCsv()
           .map { row -> tuple(row[0], file(row[1])) }
           .set{ refs }

    Channel.fromPath( "${params.inputFast5s}" )
           .splitCsv()
           .map { row -> tuple(row[0], file("${row[1]}/*.fast5")) }
           .transpose()
           .map { row -> tuple(row[0], row[1].simpleName, file(row[1])) }
           //.view()
           .set{ ch_indv_f5s }

    main:
    dorado(ch_indv_f5s)

    guppy(ch_f5s, refs)
    

}