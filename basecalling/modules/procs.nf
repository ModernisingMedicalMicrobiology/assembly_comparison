

process download_models {
    
    output:
    path("dna_r10.4.1_e8.2_400bps_fast@v4.0.0"), emit: fast
    path("dna_r10.4.1_e8.2_400bps_sup@v4.1.0"), emit: sup    
    path("dna_r10.4.1_e8.2_400bps_sup@v4.2.0_6mA@v2"), emit: sup_6mA
    path("dna_r10.4.2_e8.2_4khz_stereo@v1.0"), emit: stereo

    script:
    """
    dorado download --model  dna_r10.4.1_e8.2_400bps_fast@v4.0.0 
    /mnt/data/soft/dorado/dorado-0.3.2-linux-x64/bin/dorado download --model  dna_r10.4.1_e8.2_400bps_sup@v4.1.0
    dorado download --model  dna_r10.4.2_e8.2_4khz_stereo@v1.0
    /mnt/data/soft/dorado/dorado-0.3.2-linux-x64/bin/dorado download --model  dna_r10.4.1_e8.2_400bps_sup@v4.2.0_6mA@v2
    """
    stub:
    """
    touch dna_r10.4.1_e8.2_400bps_fast@v4.0.0 dna_r10.4.1_e8.2_400bps_sup@v4.0.0 dna_r10.4.2_e8.2_4khz_stereo@v1.0
    """

}

process f5ToPod5 {
    tag {sample + ' ' + fileName}

    //maxForks 1

    input:
    tuple val(sample), val(fileName), path('file.fast5')

    output:
    tuple val(sample), val(fileName), path('pod5s/')

    script:
    """
    mkdir f5s pod5s
    mv *.fast5 f5s/
    /pod5/bin/pod5 convert fast5 f5s/ pod5s/
    """
    stub:
    """
    touch file.pod5
    """


}

process simplex_basecall {
    tag {sample + ' ' + fileName}

    label 'gpu'
    //maxForks 1

    publishDir "bams/dorado/${task.process.replaceAll(":","_")}", saveAs: { filename -> "${sample}_${fileName}.bam"}


    input:
    tuple val(sample), val(fileName), path('pod5s'), path('model'),path('mod_model')
    
    output:
    tuple val(sample), val(fileName), path("${sample}.bam"), emit: fileNameBam
    tuple val(sample), path("${sample}.bam"), emit: bam


    script:
    """
    /mnt/data/soft/dorado/dorado-0.3.2-linux-x64/bin/dorado basecaller  \
        model pod5s/ \
        --device ${params.doradocudadevice} \
        --modified-bases 5mCG_5hmCG > ${sample}.bam
    """
    stub:
    """
    touch unmapped_reads_with_moves.sam
    """
}

process merge_dorado_bams {
    tag {sample}

    publishDir "bams/${task.process.replaceAll(":","_")}"


    input:
    tuple val(sample), path('in.*.bam')

    output:
    //tuple val(sample), path("${sample}.bam"), path("${sample}.bam.bai"), emit: bam
    tuple val(sample), path("${sample}_namesort.bam"), emit: bamNameSort


    script:
    """
    samtools merge ${sample}.bam in*.bam 
    samtools sort -n -o ${sample}_namesort.bam ${sample}.bam
    """
    stub:
    """
    touch ${sample}_namesort.bam
    """
}

process modkit_dorado {
    tag {sample}
    cpus 4

    publishDir "beds/${task.process.replaceAll(":","_")}"

    input:
    tuple val(sample), path("${sample}.bam"), path("${sample}.bam.bai")

    output:
    tuple val(sample), path("${sample}.bed"), emit: bed

    script:
    """
    /mnt/data/soft/modkit/dist/modkit pileup \
        ${sample}.bam \
        ${sample}.bed \
        -t ${task.cpus} 

    """
    stub:
    """
    touch ${sample}.bed
    """
}

process find_pairs {
    tag {sample + ' ' + fileName}

    input:
    tuple val(sample), val(fileName), path("unmapped_reads_with_moves.sam")
    output:
    tuple val(sample), val(fileName), path("pairs_from_bam/pair_ids_filtered.txt")

    script:
    """
    /duplex_tools/bin/duplex_tools pair \
        unmapped_reads_with_moves.sam \
        --output_dir pairs_from_bam/
    """
    stub:
    """
    mkdir pairs_from_bam
    touch pairs_from_bam/pair_ids_filtered.txt
    """

}

process find_additional_pairs {
    tag {sample + ' ' + fileName}

    errorStrategy 'ignore'

    input:
    tuple val(sample), val(fileName), path("unmapped_reads_with_moves.sam"), path('pod5s/')
    output:
    tuple val(sample), val(fileName), path("split_duplex_pair_ids.txt")

    script:
    """
    /duplex_tools/bin/duplex_tools split_pairs \
        unmapped_reads_with_moves.sam \
        pod5s/ pod5s_splitduplex/
    cat pod5s_splitduplex/*_pair_ids.txt > split_duplex_pair_ids.txt
    """
    stub:
    """
    touch split_duplex_pair_ids.txt
    """

}

process stereo_basecall {
    tag {sample + ' ' + fileName}

    label 'gpu'
    //maxForks 1

    input:
    tuple val(sample), val(fileName), path('pod5s'), path('model'), path('pair_ids_filtered.txt'), path('dna_r10.4.2_e8.2_4khz_stereo@v1.0')
    
    output:
    tuple val(sample), val(fileName), path("duplex_orig.sam")

    script:
    """
    dorado duplex model \
        pod5s/ \
        --pairs pair_ids_filtered.txt \
        --batchsize 128 \
        --device ${params.doradocudadevice} \
        > duplex_orig.sam
    
    """
    stub:
    """
    touch duplex_orig.sam
    """

}

process stereo_basecall_additional {
    tag {sample + ' ' + fileName}

    label 'gpu'
    //maxForks 1

    input:
    tuple val(sample), val(fileName), path('file.fast5'), path('model'), path('split_duplex_pair_ids.txt'), path('dna_r10.4.2_e8.2_4khz_stereo@v1.0')
    
    output:
    tuple val(sample), val(fileName), path("duplex_splitduplex.sam")

    script:
    """
    mkdir f5s
    mv *.fast5 f5s/
    dorado duplex \
        model \
        f5s/ \
        --pairs split_duplex_pair_ids.txt \
        --device ${params.doradocudadevice} > duplex_splitduplex.sam
    
    """
    stub:
    """
    touch duplex_splitduplex.sam
    """
}

process samToBamFastq {
    tag {sample + ' ' + fileName}

    publishDir "bams/${task.process.replaceAll(":","_")}/${sample}/", mode: 'copy', pattern: '*.bam'
    publishDir "fastqs/${task.process.replaceAll(":","_")}/${sample}/", mode: 'copy', pattern: '*.fastq.gz'

    input:
    tuple val(sample), val(fileName), path("in.sam")

    output:
    tuple val(sample), val(fileName), path("${fileName}.bam"), emit: bam
    tuple val(sample), val(fileName), path("${fileName}.fastq.gz"), emit: fastq

    script:
    """
    samtools view -bS in.sam > ${fileName}.bam
    samtools bam2fq ${fileName}.bam | gzip > ${fileName}.fastq.gz
    """
    stub:
    """
    touch "${fileName}.bam" "${fileName}.fastq.gz"
    """

}

// GUPPY 

process guppy_fast {
    tag {sample}

    label 'gpu'
    //maxForks 1

    input:
    tuple val(sample), path('fast5s/')

    output:
    tuple val(sample), path('basecalled')

    script:
    """
    guppy_basecaller -r -i fast5s/ \
        -s basecalled \
        --device ${params.cudadevice} \
        -c dna_r10.4.1_e8.2_400bps_sup.cfg
    """
    stub:
    """
    mkdir basecalled
    touch basecalled/sequencing_summary.txt
    """

}

process guppy_methylation {
    tag {sample}

    label 'gpu'
    publishDir "bams/${task.process.replaceAll(":","_")}"

    //maxForks 1

    input:
    tuple val(sample), path('fast5s/'), path('ref.fa')

    output:
    tuple val(sample), path('basecalled'), emit: fol 

    script:
    """
    guppy_basecaller \
        --config ${params.methylation_model} \
        --device ${params.cudadevice} \
        --bam_out --recursive  \
        --align_ref ref.fa \
        -i fast5s/  -s basecalled

    """
    stub:
    """
    mkdir basecalled
    touch basecalled/sequencing_summary.txt
    """

}

process merge_bams {
    tag {sample}

    input:
    tuple val(sample), path('basecalled')

    output:
    tuple val(sample), path("${sample}.bam"), path("${sample}.bam.bai"), emit: bam

    script:
    """
    samtools merge ${sample}.bam basecalled/pass/bam*.bam 
    samtools index ${sample}.bam
    """
    stub:
    """
    touch ${sample}.bam ${sample}.bam.bai
    """
}

process modbam2bed {
    tag {sample + ' ' + mod}
    cpus 4

    publishDir "beds/${task.process.replaceAll(":","_")}"

    input:
    tuple val(sample), path("${sample}.bam"), path("${sample}.bam.bai"), path('ref.fa'), val(mod)

    output:
    tuple val(sample), path("${sample}.${mod}.bed"), emit: bed

    script:
    """
    /home/nick/miniconda3/envs/modbam2bed/bin/modbam2bed --aggregate -e \
        -p ${sample} \
        -m ${mod} \
        -t ${task.cpus} \
        ref.fa \
        ${sample}.bam \
        > ${sample}.${mod}.bed
    """
    stub:
    """
    touch ${sample}.cpg.bed
    """
}

process modkit {
    tag {sample}
    cpus 4

    publishDir "beds/${task.process.replaceAll(":","_")}"

    input:
    tuple val(sample), path("${sample}.bam"), path("${sample}.bam.bai"), path('ref.fa')

    output:
    tuple val(sample), path("${sample}.bed"), emit: bed

    script:
    """
    /mnt/data/soft/modkit/dist/modkit pileup \
        ${sample}.bam \
        ${sample}.bed \
        --ref ref.fa \
        -t ${task.cpus} 

    """
    stub:
    """
    touch ${sample}.bed
    """
}




process find_pairs_guppy {
    tag {sample}

    input:
    tuple val(sample), path("basecalled")
    output:
    tuple val(sample), path("pairs/pair_ids.txt")

    script:
    """
    /duplex_tools/bin/duplex_tools pairs_from_summary \
        basecalled/sequencing_summary.txt \
        pairs
    """
    stub:
    """
    mkdir pairs
    touch pairs/pair_ids.txt
    """

}

process filter_pairs {
    tag {sample}

    input:
    tuple val(sample), path("basecalled"), path('pair_ids.txt')
    output:
    tuple val(sample), path("pair_ids_filtered.txt")

    script:
    """
    /duplex_tools/bin/duplex_tools filter_pairs pair_ids.txt basecalled/pass/
    """
    stub:
    """
    touch pair_ids_filtered.txt
    """

}

process guppy_duplex {
    tag {sample}

    label 'gpu'
    //maxForks 1

    input:
    tuple val(sample), path('fast5s'), path('pair_ids.txt'), path("pair_ids_filtered.txt")

    output:
    tuple val(sample), path('duplex_calls')

    script:
    """
    guppy_basecaller_duplex \
        -i fast5s \
        -r -s duplex_calls \
        --device ${params.cudadevice} -c dna_r10.4.1_e8.2_400bps_sup.cfg \
        --chunks_per_runner 128 \
        --duplex_pairing_mode from_pair_list \
        --duplex_pairing_file pair_ids_filtered.txt
    """
    stub:
    """
    mkdir duplex_calls
    """

}

process cat_fqs {
    tag {sample + ' ' + model }

    publishDir "fastqs/${task.process.replaceAll(":","_")}", mode: 'copy'


    input:
    tuple val(sample), path('calls'), val(model)

    output:
    path("${sample}_${model}.fastq.gz")

    script:
    """
    cat calls/pass/* | gzip > ${sample}_${model}.fastq.gz
    """
    stub:
    """
    touch ${sample}_${model}.fastq.gz
    """

}